// Copyright (C) 2010-2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// Updates the .xml files by deleting existing files and
// creating them again from the .sci with help_from_sci.


//
cwd = get_absolute_file_path("update_help.sce");
mprintf("Working dir = %s\n",cwd);
//
// Generate the library help
mprintf("Updating number\n");
helpdir = cwd;
funmat = [
  "number_addgroupmod"
  "number_coprime"
  "number_eulertotient"
  "number_extendedeuclid"
  "number_gcd"
  "number_inversemod"
  "number_isdivisor"
  "number_lcm"
  "number_powermod"
  "number_productmod"
  "number_solvelinmod"
  "number_ulamplot"
  "number_ulamspiral"
  ];
macrosdir = cwd +"../../macros";
demosdir = [];
modulename = "number";
helptbx_helpupdate ( funmat , helpdir , macrosdir , demosdir , modulename , %t );
//
// Generate the conversion help
mprintf("Updating conversion\n");
helpdir = cwd+ "conversion";
funmat = [
  "number_barygui"
  "number_baryguiclose"
  "number_frombary"
  "number_tobary"
  "number_hex2bin"
  "number_bin2hex"
  ];
macrosdir = cwd +"../../macros";
demosdir = [];
modulename = "number";
helptbx_helpupdate ( funmat , helpdir , macrosdir , demosdir , modulename , %t );
//
// Generate the special help
mprintf("Updating special\n");
helpdir = cwd+ "special";
funmat = [
  "number_carmichael"
  "number_fermat"
  "number_mersenne"
  ];
macrosdir = cwd +"../../macros";
demosdir = [];
modulename = "number";
helptbx_helpupdate ( funmat , helpdir , macrosdir , demosdir , modulename , %t );
//
// Generate the primes help
mprintf("Updating primes\n");
helpdir = cwd+ "primes";
funmat = [
  "number_primes100"
  "number_primes1000"
  "number_primes10000"
  "number_isprime"
  "number_multiprimality"
  "number_maximalprimegap"
  "number_primes"
  "number_primecount"
  "number_primorial"
  "number_probableprime"
  "number_pseudoprime"
  ];
macrosdir = cwd +"../../macros";
demosdir = [];
modulename = "number";
helptbx_helpupdate ( funmat , helpdir , macrosdir , demosdir , modulename , %t );

//
// Generate the factorization help
mprintf("Updating factorization\n");
helpdir = cwd+ "factorization";
funmat = [
  "number_factor"
  "number_getfactors"
  "number_getprimefactors"
  ];
macrosdir = cwd +"../../macros";
demosdir = [];
modulename = "number";
helptbx_helpupdate ( funmat , helpdir , macrosdir , demosdir , modulename , %t );
//
// Generate the multgroup help
mprintf("Updating multgroup\n");
helpdir = cwd+ "multgroup";
funmat = [
  "number_isprimroot"
  "number_primitiveroot"
  "number_multgroupmod"
  "number_multorder"
  ];
macrosdir = cwd +"../../macros";
demosdir = [];
modulename = "number";
helptbx_helpupdate ( funmat , helpdir , macrosdir , demosdir , modulename , %t );
