<?xml version="1.0" encoding="UTF-8"?>

<!--
 *
 * This help file was generated from number_primitiveroot.sci using help_from_sci().
 *
 -->

<refentry version="5.0-subset Scilab" xml:id="number_primitiveroot" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">


  <refnamediv>
    <refname>number_primitiveroot</refname><refpurpose>Find a primitive root modulo m.</refpurpose>
  </refnamediv>



<refsynopsisdiv>
   <title>Calling Sequence</title>
   <synopsis>
   G=number_primitiveroot(m)
   G=number_primitiveroot(m,"smallest")
   G=number_primitiveroot(m,imax)
   G=number_primitiveroot(m,imax,selectfun)
   
   </synopsis>
</refsynopsisdiv>

<refsection>
   <title>Parameters</title>
   <variablelist>
   <varlistentry><term>m :</term>
      <listitem><para> a 1-by-1 matrix of floating point integers, the modulo</para></listitem></varlistentry>
   <varlistentry><term>imax :</term>
      <listitem><para> a 1-by-1 matrix of doubles or strings (default imax=1), the maximum number of primitive roots to compute.</para></listitem></varlistentry>
   <varlistentry><term>selectfun :</term>
      <listitem><para> a function or a list, a selection function (default selectfun=[], which means that all primitive roots are selected). See below for details.</para></listitem></varlistentry>
   <varlistentry><term>G :</term>
      <listitem><para> a sg_by-1 matrix floating point integers, the primitive roots modulo m.</para></listitem></varlistentry>
   </variablelist>
</refsection>

<refsection>
   <title>Description</title>
   <para>
This function computes primitive roots modulo m.
If there is no primitive root modulo m, returns
the empty matrix [].
   </para>
   <para>
By default, we have imax==1, so that we search a primitive
root modulo m.
In this case, the computed primitive root is not
necessarily the smallest.
Moreover, the selectfun input argument is ignored if imax==1.
   </para>
   <para>
The calling sequence G=number_primitiveroot(m,"smallest")
computes the smallest primitive root modulo m.
   </para>
   <para>
If imax>1, we stop the search when we have found imax primitive roots.
In this case, the primitive roots are in increasing order and
G(1) is the smallest possible primitive root.
Moreover, only the primitive roots a which are so that selectfun(a)
is true are kept in G.
   </para>
   <para>
If imax is infinite, we stop the search
when all primitive roots have been found.
   </para>
   <para>
The function selectfun should have header
   </para>
   <para>
<screen>
tf = selectfun( a )
</screen>
   </para>
   <para>
where
a is a floating point integer, representing a primitive root,
tf is a 1-by-1 matrix of booleans.
If tf is true, then the primitive root a is included in the matrix G.
   </para>
   <para>
It might happen that the function requires additionnal
arguments to be evaluated.
In this case, we can use the following feature.
The argument selectfun can also be a list, where the first
item is a function f.
The function f must have the header
   </para>
   <para>
<screen>
tf = f ( a , a1 , a2 , ... ).
</screen>
   </para>
   <para>
In this case the selectfun variable should hold the list (f,a1,a2,...) and the
input arguments a1, a2, will be automatically be appended at the
end of the calling sequence of f.
   </para>
   <para>
Definition of a primitive root
   </para>
   <para>
Let a be an integer relatively prime to m.
If the order of a modulo m is number_eulertotient(m),
then    a is a primitive root modulo m.
In other words, if
   </para>
   <para>
<screen>
number_multorder(a,m)==number_eulertotient(m)
</screen>
   </para>
   <para>
then a is a primitive root modulo m.
   </para>
   <para>
If there exists a primitive root modulo m, then
the number of primitive roots modulo m is
   </para>
   <para>
<screen>
number_eulertotient(number_eulertotient(m))
</screen>
   </para>
   <para>
Notes on the performances of the algorithm
   </para>
   <para>
The fastest performance is achieved with imax==1 and selectfun is empty (i.e.
with the default settings).
This is because the returned primitive root does not have
to be the smallest.
   </para>
   <para>
The calling sequence G=number_primitiveroot(m,"smallest")
requires that the primitive root is the smallest.
This implies that all candidates a=1,2,... have to be
tried before a primitive root can be found.
For small values of m, this is fast.
But for large values of m, this may require
a large number of iterations.
   </para>
   <para>
</para>
</refsection>

<refsection>
   <title>Examples</title>
   <programlisting role="example"><![CDATA[
// Check that a=5 is the only primitive root modulo 6.
G = number_primitiveroot ( 6 )
G = number_primitiveroot ( 6 , %inf )

// Find all the primitive roots modulo 11
G = number_primitiveroot ( 11 , %inf )
expected = [2 6 7 8]'
// There is more that on primitive root.
// Therefore, the number of primitive roots is: 4
number_eulertotient(number_eulertotient(11))

// There is no primitive root modulo 24
G = number_primitiveroot ( 24 , %inf )

// Find one primitive root modulo 218: 115
G = number_primitiveroot ( 218 )
// Find the smallest primitive root modulo 218: 11
G = number_primitiveroot ( 218 , "smallest" )

// Find one primitive root modulo 223 : 3
G = number_primitiveroot ( 223 )

// Print the primitive roots modulo m
// for m=1,2,...,15
for m = 1 : 15
X = number_primitiveroot(m,%inf);
mprintf("Primitive roots mod %d: %s\n",..
m,strcat(string(X),","));
end

// Print the smallest primitive roots modulo m
// for m=1,2,...,227, if any
for m = 1 : 50
g = number_primitiveroot(m,"smallest");
if ( g<>[] ) then
mprintf("Primitive root mod %d: %d\n",..
m,g);
end
end

// Find the smallest prime primitive root modulo 3545281
// This is 163
function tf = selectprime(a)
tf = number_isprime(a)
endfunction
number_primitiveroot(3545281,"smallest",selectprime)

   ]]></programlisting>
</refsection>

<refsection>
   <title>Bibliography</title>
   <para>http://en.wikipedia.org/wiki/Primitive_root_modulo_n</para>
   <para>http://people.cs.kuleuven.be/~dirk.nuyens/code/generatorp.m</para>
   <para>An Introduction to the Theory of Numbers, 5th Edition, Ivan Niven, Herbert S. Zuckerman, Hugh L. Montgomery, 1991, John Wiley &amp; Sons</para>
   <para>The primitive root theorem, Amin Witno, 2012, WON Series in Discrete Mathematics and Modern Algebra Volume 5</para>
</refsection>

<refsection>
   <title>Authors</title>
   <simplelist type="vert">
   <member>Copyright (C) 2012 - Michael Baudin</member>
   <member>Copyright (C) 2004 - Dirk Nuyens</member>
   </simplelist>
</refsection>
</refentry>
