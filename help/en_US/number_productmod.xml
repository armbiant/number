<?xml version="1.0" encoding="UTF-8"?>

<!--
 *
 * This help file was generated from number_productmod.sci using help_from_sci().
 *
 -->

<refentry version="5.0-subset Scilab" xml:id="number_productmod" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">


  <refnamediv>
    <refname>number_productmod</refname><refpurpose>Computes a*s modulo m.</refpurpose>
  </refnamediv>



<refsynopsisdiv>
   <title>Calling Sequence</title>
   <synopsis>
   p = number_productmod ( a , s , m )

   </synopsis>
</refsynopsisdiv>

<refsection>
   <title>Parameters</title>
   <variablelist>
   <varlistentry><term>a :</term>
      <listitem><para> a 1x1 matrix of floating point integers.</para></listitem></varlistentry>
   <varlistentry><term>s :</term>
      <listitem><para> a 1x1 matrix of floating point integers.</para></listitem></varlistentry>
   <varlistentry><term>m :</term>
      <listitem><para> a 1x1 matrix of floating point integers, the modulo</para></listitem></varlistentry>
   <varlistentry><term>p :</term>
      <listitem><para> a 1x1 matrix of floating point integers, the result of a*s modulo m.</para></listitem></varlistentry>
   </variablelist>
</refsection>

<refsection>
   <title>Description</title>
   <para>
Returns the result of a*s (modulo m).
If the arguments a or s are too large, an error is generated.
   </para>
   <para>
It may happen the intermediate product a*s become too large than the
largest accurately representable integer value (i.e. larger than
2^53).
We use algorithm which prevent potential overflows of the
intermediate values.
Hence, if a, s and m are exactly representable, so is p.
   </para>
   <para>
The algorithm automatically switches to three
different methods, depending on the value of a, s and m.
   </para>
   <para>
If no overflow can occur, the formula pmodulo(a*s,m)
is used.
   </para>
   <para>
The "shrage" algorithm uses Shrage's method and guarantees
that all intermediate values remain in the range
from -m to m (inclusive).
This algorithm only works if a^2&lt;=m or s^2&lt;=m.
   </para>
   <para>
The "reduction" algorithm uses recursive reduction.
More precisely, it uses Shrage's algorithm,
and recursively uses it for the expression
floor(s/q)*r, if it may overflow.
The actual algorithm is iterative (and not recursive).
This is an algorithm from L'Ecuyer and Côté (1991).
This algorithm works whatever the value of a, s and m.
   </para>
   <para>
</para>
</refsection>

<refsection>
   <title>Examples</title>
   <programlisting role="example"><![CDATA[
p = number_productmod ( 5 , 3 , 31 )
p2 = modulo(5*3,31)

p = number_productmod ( 6 , 4 , 41 )
p2 = modulo(6*4,41)

p = number_productmod ( 3 , 1 , 11 )
p2 = modulo(3*1,11)

// The algorithm works with large numbers:
p = number_productmod(106034106,106034106,137568061)
expected = 86644614

p = number_productmod(2^16,2^15,2^50)
expected = 2147483648

p = number_productmod(2^16,(2^47-1),2^50)
expected = 1125899906777088

// Works also for negative a
number_productmod ( -6 , 4 , 41 )
// ... or negative s ...
number_productmod ( 6 , -4 , 41 )
// ... or negative m ...
number_productmod ( 6 , 4 , -41 )

   ]]></programlisting>
</refsection>

<refsection>
   <title>Bibliography</title>
   <para>A guide to simulation, 2nd Edition, Bratley, Fox, Shrage, Springer-Verlag, New York, 1987</para>
   <para>Implementing a Random Number Package with Splitting Facilities, Pierre L'Ecuyer, Serge Cote, ACM TOMS, Vol. 17, No.1, March 1991, Pages 98-111</para>
</refsection>

<refsection>
   <title>Authors</title>
   <simplelist type="vert">
   <member>Copyright (C) 2012 - Michael Baudin</member>
   </simplelist>
</refsection>
</refentry>
