<?xml version="1.0" encoding="UTF-8"?>

<!--
 *
 * This help file was generated from number_probableprime.sci using help_from_sci().
 *
 -->

<refentry version="5.0-subset Scilab" xml:id="number_probableprime" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">


  <refnamediv>
    <refname>number_probableprime</refname><refpurpose>Check if a number is a probable prime.</refpurpose>
  </refnamediv>



<refsynopsisdiv>
   <title>Calling Sequence</title>
   <synopsis>
   isprime = number_probableprime ( n )
   isprime = number_probableprime ( n , s )
   isprime = number_probableprime ( n , s , verbose )
   
   </synopsis>
</refsynopsisdiv>

<refsection>
   <title>Parameters</title>
   <variablelist>
   <varlistentry><term>n :</term>
      <listitem><para> a 1x1 matrix of floating point integers, must be positive, the positive integer to test for primality</para></listitem></varlistentry>
   <varlistentry><term>s :</term>
      <listitem><para> a 1x1 matrix of floating point integers, the number of loops in the test for primality (default 50)</para></listitem></varlistentry>
   <varlistentry><term>verbose :</term>
      <listitem><para> a 1-by-1 matrix of booleans, set to true to display messages (default verbose=%f)</para></listitem></varlistentry>
   <varlistentry><term>isprime :</term>
      <listitem><para> a 1x1 matrix of booleans</para></listitem></varlistentry>
   </variablelist>
</refsection>

<refsection>
   <title>Description</title>
   <para>
Returns %f if the number is a composite (surely).
Returns %t if the number is a probable prime (with a small probability of error).
   </para>
   <para>
This function uses Miller-Rabin randomized primality test, as
described in "Introduction to algorithms" by Cormen, Leiserson, Rivest, Stein
(see the section 31.8 Primality testing).
   </para>
   <para>
If number_probableprime returns %f, therefore we know (i.e. with probability 1)
that n is composite.
   </para>
   <para>
If number_probableprime returns %t, therefore we know that n is
a probable prime (with a small probability of error),
but we do not claim that n is prime.
In fact, if n is a probable prime, then the probability
that n is not prime is 2^-s (see Theorem 31.39 in "Introduction to algorithms").
With the default value s=50, the probability that number_probableprime fails is
9e-16, i.e. very small.
   </para>
   <para>
This function uses random integers from the grand function (option "uin").
   </para>
   <para>
</para>
</refsection>

<refsection>
   <title>Examples</title>
   <programlisting role="example"><![CDATA[
// Check for actual primes
number_probableprime ( 7 ) // %t
number_probableprime ( 5 ) // %t
number_probableprime ( 5 , 3 ) // %t

// Check for composite numbers
number_probableprime ( 10 ) // %f
number_probableprime ( 20 ) // %f

// Check for pseudo-primes : while pseudo prime does not detect that these numbers
// are composite, probable prime does it perfectly.
number_probableprime ( 341 ) // %f
number_probableprime ( 561 ) // %f
number_probableprime ( 645 ) // %f
number_probableprime ( 1105 ) // %f

// Test verbose option
number_probableprime ( 10001 , [] , %t );

// Test s option
number_probableprime ( 10001 , 10 , %t );

// This function may fail if intermediate integers get too large.
// An error is generated:
number_probableprime ( 4432676798593 )

   ]]></programlisting>
</refsection>

<refsection>
   <title>Bibliography</title>
   <para>"Introduction to algorithms", Cormen, Leiserson, Rivest, Stein, 2nd edition</para>
   <para>http://en.wikipedia.org/wiki/Miller-Rabin_primality_test</para>
</refsection>

<refsection>
   <title>Authors</title>
   <simplelist type="vert">
   <member>Copyright (C) 2009 - 2010 - DIGITEO - Michael Baudin</member>
   </simplelist>
</refsection>
</refentry>
