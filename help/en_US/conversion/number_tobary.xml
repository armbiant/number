<?xml version="1.0" encoding="UTF-8"?>

<!--
 *
 * This help file was generated from number_tobary.sci using help_from_sci().
 *
 -->

<refentry version="5.0-subset Scilab" xml:id="number_tobary" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">


  <refnamediv>
    <refname>number_tobary</refname><refpurpose>Decompose a number into arbitrary basis.</refpurpose>
  </refnamediv>



<refsynopsisdiv>
   <title>Calling Sequence</title>
   <synopsis>
   digits = number_tobary ( n )
   digits = number_tobary ( n , basis )
   digits = number_tobary ( n , basis , order )
   digits = number_tobary ( n , basis , order , p )

   </synopsis>
</refsynopsisdiv>

<refsection>
   <title>Parameters</title>
   <variablelist>
   <varlistentry><term>n :</term>
      <listitem><para> a 1x1 matrix of floating point integers, the integer to decompose, must be positive</para></listitem></varlistentry>
   <varlistentry><term>basis :</term>
      <listitem><para> a 1x1 matrix of floating point integers, the basis, must be positive, (default basis=2)</para></listitem></varlistentry>
   <varlistentry><term>order :</term>
      <listitem><para> a 1x1 matrix of strings, the order (default order="littleendian")</para></listitem></varlistentry>
   <varlistentry><term>p :</term>
      <listitem><para> a 1x1 matrix of floating point integers, the number of digits on output (default p=0)</para></listitem></varlistentry>
   <varlistentry><term>digits :</term>
      <listitem><para> a p-by-1 matrix of floating point integers, the digits.</para></listitem></varlistentry>
   </variablelist>
</refsection>

<refsection>
   <title>Description</title>
   <para>
Returns a column matrix of digits of the decomposition of
n in base b, i.e. decompose n as
<programlisting>
n = d(1) b^(p-1) + d(2) b^(p-2) + ... + d(p) b^0.
</programlisting>
This order is little endian order since the last digit
is associated with b^0.
If "bigendian" order is chosen, returns the digits d so that
<programlisting>
n = d(1) b^0 + d(2) b^1 + ... + d(p) b^(p-1),
</programlisting>
i.e. the last digit is associated with b^(p-1).
   </para>
   <para>
If p=0, returns the least possible number of digits.
If p is a positive integer, returns p digits.
If the integer cannot be stored in p digits, generates an error.
If the integer is larger than p digits, pad with zeros.
   </para>
   <para>
</para>
</refsection>

<refsection>
   <title>Examples</title>
   <programlisting role="example"><![CDATA[
number_tobary (4,2) // [1 0 0]
number_tobary (4,2,"bigendian") // [0 0 1]
number_tobary (4,2,"littleendian",8) // [0,0,0,0,0,1,0,0]

   ]]></programlisting>
</refsection>

<refsection>
   <title>Bibliography</title>
   <para>Monte-Carlo methods in Financial Engineering, Paul Glasserman</para>
</refsection>

<refsection>
   <title>Authors</title>
   <simplelist type="vert">
   <member>Copyright (C) 2008-2009 - INRIA - Michael Baudin</member>
   <member>Copyright (C) 2010 - DIGITEO - Michael Baudin</member>
   </simplelist>
</refsection>
</refentry>
