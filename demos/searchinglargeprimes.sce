// Copyright (C) 2009 - Digiteo - Michael Baudin

//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function demoSearchprimes()

    mprintf ( "Search for learge primes\n")
    for i = 1:2:100
        mprintf ( "Testing 2^53 - %d...\n" , i )
        [isprime,status] = number_isprime ( 2^53 - i , "210k" );
        if ( isprime ) then
            mprintf ( "2^53-%d is prime\n", i);
            break
        else
            mprintf ( "> Not prime (status = %s)\n", string(status) )
        end
    end

endfunction
demoSearchprimes();
clear demoSearchprimes
