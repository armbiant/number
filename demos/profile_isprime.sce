// Copyright (C) 2009 - Digiteo - Michael Baudin

//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

//
// Profile isprime
//
// The number 4432676798593 is a prime.
// The "primes" method, which requires to compute all primes up to sqrt(n) = 2105392
// needs too much memory.
add_profiling("number_isprime")
add_profiling("isprime_modulo")
add_profiling("isprime_6k")
add_profiling("isprime_30k")
add_profiling("isprime_massdivide")
[c,s] = number_isprime ( 4432676798593 , "6k" )
plotprofile(isprime_6k)
[c,s] = number_isprime ( 4432676798593 , "30k" )
plotprofile(isprime_30k)
// Mass division : perfect !
[c,s] = number_isprime ( 4432676798593 , "massdivide" )
plotprofile(isprime_30k)
[c,s] = number_isprime ( 4432676798593 , "modulo" )
plotprofile(isprime_modulo)

