// Copyright (C) 2010 - Michael Baudin

//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function demoPrimegaphistplot()
    //
    // References
    // http://sites.google.com/site/primenumbergaps/
	stacksize("max")
    n=1.e7;
    mprintf("Plots the distribution of prime gaps\n")
    mprintf("For all primes lower than n=%d\n",n)
    allprimes = number_primes(n);
    gaps = diff(allprimes);
    scf();
    histplot(100,gaps);
    xtitle("Distribution of prime gaps","Gap","Frequency")

    //========= E N D === O F === D E M O =========//
    //
    // Load this script into the editor
    //
    filename = "primegapdistribution.sce";
    dname = get_absolute_file_path(filename);
    editor ( fullfile(dname,filename) );

endfunction
demoPrimegaphistplot();
clear demoPrimegaphistplot
