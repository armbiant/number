// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function binstr = number_hex2bin ( hexstr )
  // Converts an hexadecimal string into a binary string.
  //
  // Calling Sequence
  //   binstr = number_hex2bin ( hexstr )
  //
  // Parameters
  //   hexstr : a matrix of strings
  //   binstr : a matrix of strings
  //
  // Description
  //   Returns the binary string corresponding to the given hexadecimal string.
  //
  // Examples
  // hexstr = "3FE54B3504C6B4A3";
  // binstr = number_hex2bin (hexstr);
  // expected = "0011111111100101010010110011010100000100110001101011010010100011"
  //
  // // Lower case also works
  // hexstr = "3fe54b3504c6b4a3";
  // binstr = number_hex2bin (hexstr);
  // expected = "0011111111100101010010110011010100000100110001101011010010100011"
  //
  // Authors
  // Copyright (C) 2010 - DIGITEO - Michael Baudin
  //

  [lhs, rhs] = argn()
  apifun_checkrhs ( "number_hex2bin" , rhs , 1 )
  apifun_checklhs ( "number_hex2bin" , lhs , 1 )
  //
  apifun_checktype ( "number_hex2bin" , hexstr , "hexstr" , 1 , "string" )
  //
  // Move to upper case
  hexstr = convstr(hexstr,"u")
  //
  hexmap = [
    "0" "0000"
    "1" "0001"
    "2" "0010"
    "3" "0011"
    "4" "0100"
    "5" "0101"
    "6" "0110"
    "7" "0111"
    "8" "1000"
    "9" "1001"
    "A" "1010"
    "B" "1011"
    "C" "1100"
    "D" "1101"
    "E" "1110"
    "F" "1111"
  ]
  for ir = 1 : size(hexstr,"r")
  for ic = 1 : size(hexstr,"c")
    binstr(ir,ic)=""
    for k = 1 : length(hexstr(ir,ic))
      hexchar = part(hexstr,k)
      binind = find(hexmap(:,1)==hexchar)
      if ( binind==[] ) then
        error(msprintf(gettext("%s: Letter %s at entry #%d at indices (%d,%d) in the matrix is not hexadecimal."),"number_hex2bin",hexchar,k,ir,ic))
      end
      binchar = hexmap(binind,2)
      binstr(ir,ic) = binstr(ir,ic) + binchar
    end
  end
  end
endfunction

