// Copyright (C) 2009 - 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function number_checkrange ( n )
  // Checks that the number is in a valid range.
  //
  // Calling Sequence
  //   number_checkrange ( n )
  //
  // Parameters
  //   n : a 1x1 matrix of floating point integers
  //
  // Description
  //   Check that the input integer is in the range of managable integers.
  //   Check that it is in the interval [-2^53,2^53]
  //
  // Examples
  // format("v",25)
  // // See that 2^53 is the maximum manageable floating point integer.
  // nmax = 2^53
  // // See that the gaps between the numbers is becoming 2 over nmax
  // t = (nmax-2:nmax+2)
  // diff(t)
  // // See that we cannot perform regular algebra beyond that point.
  // nmax+1 == nmax
  //
  // // See that -2^53 is the maximum manageable floating point integer.
  // nmin = -2^53
  // // See that the gaps between the numbers is becoming 2 below nmin
  // t = (nmin-2:nmin+2)
  // diff(t)
  // // See that we cannot perform regular algebra beyond that point.
  // nmin+1 == nmin
  //
  // Authors
  // Copyright (C) 2009 - 2010 - DIGITEO - Michael Baudin
  //

  if ( ( n < -2^53 ) | ( n > 2^53 ) ) then
    error(msprintf(gettext("%s: Wrong value for integer %s: The integer is not in the bounds [-2^53,2^53].\n"),"number_checkrange" , string(n) ));
  end
endfunction

