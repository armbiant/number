// Copyright (C) 2012 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function tf = number_isprimroot(a,m)
    // See if a is a primitive root modulo m.
    //
    // Calling Sequence
    //   tf = number_isprimroot(a,m)
    //
    // Parameters
    //   a : a 1-by-1 matrix of floating point integers, the number to check
    //   m : a 1-by-1 matrix of floating point integers, the modulo
    //   tf : a 1-by-1 matrix of booleans, true if a is a primitive root modulo m
    //
    // Description
    //   This function returns true if a is a primitive root modulo m, that 
    //   is, if the order of a modulo m is number_eulertotient(m).
	//
	//   In other words, the output argument tf is true 
	//   if and only if :
	// 
	// <screen>
	// number_eulertotient(m)==number_multorder(a,m)
	// </screen>
    //
    //   The order of a modulo m is the smallest positive integer k such that 
    //
    //<latex>
    //\begin{eqnarray}
    //a^k \equiv 1 \pmod{m}
    //\end{eqnarray}
    //</latex>
    //
    // Examples
    // // See that a=2 is a primitive root modulo m=13
    // tf = number_isprimroot(2,13)
	// // Check that the multiplicative order of 2 modulo 13 
	// // is Euler's totient.
	// number_eulertotient(13) // 12
	// number_multorder(2,13) // 12
	//
    // // See that a=3 is a primitive root modulo m=7
    // tf = number_isprimroot(3,7)
    // // See that a=2 is a primitive root modulo m=9
    // tf = number_isprimroot(2,9)
    // // See that a=5 is a primitive root modulo m=6
    // tf = number_isprimroot(5,6)
    // // See that a=2 is NOT a primitive root modulo m=6
    // tf = number_isprimroot(2,6)
	//
	// // Find all primitive roots modulo 13
	// // These are : 2, 6, 7, 11
	// for a = 1 : 13
    //   tf = number_isprimroot(a,13);
	//   if ( tf ) then
	//     disp(a)
	//   end
	// end
    //
    // Bibliography
    // http://en.wikipedia.org/wiki/Primitive_root_modulo_n
    //
    // Authors
    // Copyright (C) 2012 - Michael Baudin

    [lhs, rhs] = argn()
    apifun_checkrhs ( "number_isprimroot" , rhs , 2 )
    apifun_checklhs ( "number_isprimroot" , lhs , 1 )
    //
    apifun_checktype ( "number_isprimroot" , a , "a" , 1 , "constant" )
    apifun_checktype ( "number_isprimroot" , m , "m" , 2 , "constant" )
    //
    apifun_checkscalar ( "number_isprimroot" , a , "a" , 1 )
    apifun_checkscalar ( "number_isprimroot" , m , "m" , 2 )
    //
    apifun_checkflint ( "number_isprimroot" , a, "a" , 1 )
    apifun_checkrange ( "number_isprimroot" , a, "a" , 1 , 0 , 2^53 )
    apifun_checkflint ( "number_isprimroot" , m, "m" , 1 )
    apifun_checkrange ( "number_isprimroot" , m, "m" , 1 , 0 , 2^53 )
    //
    // 1. Check that a^phi == 1 (mod m)
    phi = number_eulertotient(m)
    if (m==2 & a==1) then
        tf = %t
        return
    end
    r = number_powermod ( a, phi, m )
    if (r<>1) then
        tf = %f
        return
    end
    // 2. Check that a^(phi/p) == 1 (mod m)
    //    for all prime factors of phi
    p = number_factor ( phi )
    p = unique(p)
    for pk = p
        f = int(phi/pk)
        r = number_powermod ( a, f, m )
        if (r==1) then
            tf = %f
            return
        end
    end
    tf = %t
endfunction
