// Copyright (C) 2010 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function number_baryguiclose (  )
  //   Closes the bary GUI.
  //
  // Calling Sequence
  // number_baryguiclose (  )
  //
  // Parameters
  //
  // Description
  //   Closes the bary GUI.
  //
  //   This function is called back when the Close button of the
  //   Gui is pushed.
  //
  // Examples
  //   number_baryguiclose (  )
  //
  // Authors
  // Copyright (C) 2010 - Michael Baudin
  //

  [lhs,rhs]=argn();
  apifun_checkrhs ( "number_baryguiclose" , rhs , 0:0 )
  apifun_checklhs ( "number_baryguiclose" , lhs , 0:1 )
  //

  global number_baryguih
  close ( number_baryguih.matrixfig.figure )
  close ( number_baryguih.controlfig )
endfunction

