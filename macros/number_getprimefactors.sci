// Copyright (C) 2012 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function varargout = number_getprimefactors ( varargin )
    // Factors a number into primes and exponents.
    //
    // Calling Sequence
    //   p = number_getprimefactors ( n )
    //   p = number_getprimefactors ( n )
    //   p = number_getprimefactors ( n , method )
    //   p = number_getprimefactors ( n , method , imax )
    //   p = number_getprimefactors ( n , method , imax , verbose )
    //   p = number_getprimefactors ( n , method , imax , verbose , pollardseeds )
    //   [p,e] = number_getprimefactors ( ... )
    //   [p,e,status] = number_getprimefactors ( ... )
    //
    // Parameters
    //  n : a 1x1 matrix of floating point integers
    //  method : a 1x1 matrix of strings, the method to use (default "fasttrialdivision"). Available are method = "fasttrialdivision", "trialdivision", "memorylesstd", "fermat", "pollard-rho".
    //  imax : a 1-by-1 matrix of floating point integers, the number of steps in the loop (default imax=10000)
    //  verbose : a 1-by-1 matrix of booleans, set to true to display messages (default verbose=%f)
    //  pollardseeds : a m-by-1 matrix of floating point integers, the seeds for Pollard's rho. Only for method="pollard-rho". Default pollardseeds = [-3 1 3 5].
    //  p : a m-by-1 matrix of floating point integers. The unique prime numbers which divide n.
    //  e : a m-by-1 matrix of floating point integers. The exponents corresponding to p.
    //  status : %t if the factorization is a success, %f if the factorization is a failure
    //
    // Description
    //   Decompose n into its prime factors and their corresponding exponents.
    //   The output arguments p and e are so that each entry in p is a prime number and
    //
    //<screen>
    //prod(p.^e)==n
    //</screen>
    //
    //  In case of success, all factors are returned and success = %t.
    //
    //  In case of failure, if there is one output argument, an error
    //  is generated.
    //
    //  In case of failure, if there are two output arguments,
    //  success = %f and the factor which were found are returned in p.
    //  This feature allows to retrieve the factors which could be found (partial success), even
    //  if, globally, the algorithm failed.
    //
    //  Any input argument equal to the empty matrix is replaced by its default value.
    //
    // For details on the other input arguments, see number_factor.
    //
    // Examples
    // // p = [2 3 5], e = [3 1 1]
    // [p,e] = number_getprimefactors ( 120 )
    //
    // Bibliography
    // http://rosettacode.org/wiki/Factors_of_an_integer#Prime_factoring
    //
    // Authors
    // Copyright (C) 2012 - Michael Baudin
    //

    [lhs,rhs]=argn();
    apifun_checkrhs ( "number_getprimefactors" , rhs , 1:5 )
    apifun_checklhs ( "number_getprimefactors" , lhs , 1:3 )
    //
    // Get arguments
    n = varargin ( 1 )
    method = apifun_argindefault (  varargin , 2 , "fasttrialdivision" )
    imax = apifun_argindefault (  varargin , 3 , 10000 )
    verbose = apifun_argindefault (  varargin , 4 , %f )
    pollardseeds = apifun_argindefault (  varargin , 5 , [-3 1 3 5] )
    //
    // Check arguments
    //
    // Check type
    apifun_checktype ( "number_getprimefactors" , n , "n" , 1 , "constant" )
    apifun_checktype ( "number_getprimefactors" , method , "method" , 2 , "string" )
    apifun_checktype ( "number_getprimefactors" , imax , "imax" , 3 , "constant" )
    apifun_checktype ( "number_getprimefactors" , verbose , "verbose" , 4 , "boolean" )
    apifun_checktype ( "number_getprimefactors" , pollardseeds , "pollardseeds" , 5 , "constant" )
    //
    // Check size
    apifun_checkscalar ( "number_getprimefactors" , n , "n" , 1 )
    apifun_checkscalar ( "number_getprimefactors" , method , "method" , 2 )
    apifun_checkscalar ( "number_getprimefactors" , imax , "imax" , 3 )
    apifun_checkscalar ( "number_getprimefactors" , verbose , "verbose" , 4 )
    apifun_checkvector ( "number_getprimefactors" , pollardseeds , "pollardseeds" , 5 , size(pollardseeds,"*") )
    //
    // Check content
    apifun_checkflint ( "number_getprimefactors" , n , "n" , 1 )
    apifun_checkrange ( "number_getprimefactors" , n , "n" , 1 , -2^53 , 2^53 )
    apifun_checkoption ( "number_getprimefactors" , method , "method" , 2 , ["fasttrialdivision","trialdivision","memorylesstd","fermat","pollard-rho"] )
    apifun_checkrange ( "number_getprimefactors" , imax , "imax" , 3 , 0 , 2^53 )
    apifun_checkflint ( "number_getprimefactors" , pollardseeds , "pollardseeds" , 5 )
    apifun_checkrange ( "number_getprimefactors" , pollardseeds , "pollardseeds" , 5 , -2^53 , 2^53 )
    //
    // Proceed...
    [ fac , status ] = number_factor ( n , method , imax , verbose , pollardseeds )
	if (method=="pollard-rho") then
            localstr = gettext("%s: The algorithm may have failed to produce prime factors.")
            warning(msprintf(localstr, "number_getprimefactors" ));
	else
    if (~status) then
        localstr = gettext("%s: The algorithm failed. Try to increase the value of the imax option.")
        select lhs
        case 1 then
            error(msprintf(localstr, "number_getprimefactors" ));
        case 2 then
            error(msprintf(localstr, "number_getprimefactors" ));
        case 3 then
            varargout ( 1 ) = []
            varargout ( 2 ) = []
            varargout ( 3 ) = status
        end
        return
    end
    end
    m = size(fac,"*");
    uniquefctrs = unique(fac)
    uniquem = size(uniquefctrs,"*")
    result = ones(uniquem,2)
    for i = 1 : uniquem
        p(i) = uniquefctrs(i)
        expi = find(p(i)==fac)
        e(i) = size(expi,"*")
    end
    select lhs
    case 1 then
        varargout ( 1 ) = p
    case 2 then
        varargout ( 1 ) = p
        varargout ( 2 ) = e
    case 3 then
        varargout ( 1 ) = p
        varargout ( 2 ) = e
        varargout ( 3 ) = status
    end
endfunction
