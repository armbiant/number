// Copyright (C) 2012 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function p = number_productmod ( a , s , m )
    // Computes a*s modulo m.
    //
    // Calling Sequence
    //   p = number_productmod ( a , s , m )
    //
    // Parameters
    // a : a 1x1 matrix of floating point integers.
    // s : a 1x1 matrix of floating point integers.
    // m : a 1x1 matrix of floating point integers, the modulo
    // p : a 1x1 matrix of floating point integers, the result of a*s modulo m.
    //
    // Description
    // Returns the result of a*s (modulo m).
    // If the arguments a or s are too large, an error is generated.
    //
    // It may happen the intermediate product a*s become too large than the
    // largest accurately representable integer value (i.e. larger than
    // 2^53).
    // We use algorithm which prevent potential overflows of the
    // intermediate values.
    // Hence, if a, s and m are exactly representable, so is p.
    //
    // The algorithm automatically switches to three
    // different methods, depending on the value of a, s and m.
    //
    // If no overflow can occur, the formula pmodulo(a*s, m)
    // is used.
    //
    // The "shrage" algorithm uses Shrage's method and guarantees
    // that all intermediate values remain in the range
    // from -m to m (inclusive).
    // This algorithm only works if a^2<=m or s^2<=m.
    //
    // The "reduction" algorithm uses recursive reduction.
    // More precisely, it uses Shrage's algorithm,
    // and recursively uses it for the expression
    // floor(s/q)*r, if it may overflow.
    // The actual algorithm is iterative (and not recursive).
    // This is an algorithm from L'Ecuyer and Côté (1991).
    // This algorithm works whatever the value of a, s and m.
    //
    // Examples
    // p = number_productmod ( 5 , 3 , 31 )
    // p2 = modulo(5*3,31)
    //
    // p = number_productmod ( 6 , 4 , 41 )
    // p2 = modulo(6*4,41)
    //
    // p = number_productmod ( 3 , 1 , 11 )
    // p2 = modulo(3*1,11)
    //
    // // The algorithm works with large numbers:
    // p = number_productmod(106034106,106034106,137568061)
    // expected = 86644614
    //
    // p = number_productmod(2^16,2^15,2^50)
    // expected = 2147483648
    //
    // p = number_productmod(2^16,(2^47-1),2^50)
    // expected = 1125899906777088
    //
    // // Works also for negative a
    // number_productmod ( -6 , 4 , 41 )
    // // ... or negative s ...
    // number_productmod ( 6 , -4 , 41 )
    // // ... or negative m ...
    // number_productmod ( 6 , 4 , -41 )
    //
    // Bibliography
    // A guide to simulation, 2nd Edition, Bratley, Fox, Shrage, Springer-Verlag, New York, 1987
    // Implementing a Random Number Package with Splitting Facilities, Pierre L'Ecuyer, Serge Cote, ACM TOMS, Vol. 17, No.1, March 1991, Pages 98-111
    //
    // Authors
    // Copyright (C) 2012 - Michael Baudin

    [lhs, rhs] = argn();
    apifun_checkrhs ( "number_productmod" , rhs , 3:4 );
    apifun_checklhs ( "number_productmod" , lhs , 0:1 );

    // Check type
    apifun_checktype ( "number_productmod" , a , "a" , 1 , "constant" );
    apifun_checktype ( "number_productmod" , s , "s" , 2 , "constant" );
    apifun_checktype ( "number_productmod" , m , "m" , 3 , "constant" );

    // Check size
    apifun_checkscalar ( "number_productmod" , a , "a" , 1 );
    apifun_checkscalar ( "number_productmod" , s , "s" , 2 );
    apifun_checkscalar ( "number_productmod" , m , "m" , 3 );

    // Check content
    apifun_checkflint ( "number_productmod" , a , "a" , 1 );
    apifun_checkrange ( "number_productmod" , a , "a" , 1 , -2^53 , 2^53 );
    apifun_checkflint ( "number_productmod" , s , "s" , 2 );
    apifun_checkrange ( "number_productmod" , s , "s" , 2 , -2^53 , 2^53 );
    apifun_checkflint ( "number_productmod" , m , "m" , 3 );
    apifun_checkrange ( "number_productmod" , m , "m" , 3 , -2^53 , 2^53 );

    // Load Internals lib
    path = get_function_path("number_productmod");
    path = fullpath(fullfile(fileparts(path)));
    numberinternalslib = lib(fullfile(path,"internals"));

    // Proceed...
    p = number_productmodint(a,s,m);
endfunction


