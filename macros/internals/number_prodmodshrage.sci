// Copyright (C) 2012 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function p = number_prodmodshrage(a, s, m)
    // Returns a*s (modulo m).
    //
    // Calling Sequence
    //   p = number_prodmodshrage ( a , s , m )
    //
    // Parameters
    // a : a 1x1 matrix of floating point integers.
    // s : a 1x1 matrix of floating point integers
    // m : a 1x1 matrix of floating point integers, the modulo
    // p : a 1x1 matrix of floating point integers, the result of a*s modulo m.
    //
    // Description
    // Returns the result of a*s (modulo m).
    // This is Shrage's algorithm, which prevents unnecessary overflows.
    //
    // The algorithms makes the assumption that a>0, s>0, m>0, and a^2<=m
    // and does not check this assumption.
    //
    // Note
    // M.B. replaced the code
    // while (p<0)
    //     p = p + m
    // end
    // with:
    // p = pmodulo(p,m)
    // This is because there is a large number of
    // loops in some cases (see L'Ecuyer and Cote - 1991).
    // For example, this may happen with a, m = 2*a-1, s= m-1.
    // For example, if a=2^12, then there are 4095 loops...
    //
    // Bibliography
    // Implementing a Random Number Package with Splitting Facilities,
    // Pierre L'Ecuyer, Serge Cote,
    // ACM TOMS, Vol. 17, No.1, March 1991, Pages 98-111
    //
    // Authors
    // Copyright (C) 2012 - Michael Baudin

    if a > m then
        a = pmodulo(a, m);
    end
    if s > m then
        s = pmodulo(s, m);
    end

    q = floor(m/a);
    r = m - a*q;
    k = floor(s/q);
    t = s - k*q;
    p = a*t - k*r;
    p = pmodulo(p, m);
endfunction
