// Copyright (c) 2012 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function buildNumberInternal()
    path = get_absolute_file_path("buildmacros.sce");
    genlib("numberinternalslib",path,%f,%t);
endfunction 
buildNumberInternal();
clear buildNumberInternal
