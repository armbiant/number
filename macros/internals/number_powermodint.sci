// Copyright (C) 2012 - Michael Baudin
// Copyright (C) 2009 - 2010 - DIGITEO - Michael Baudin
// Copyright (C) 2004 - John Burkardt
//

// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// commented because it calls number_asmodmint which doesn't exist

//function x = number_powermodint ( a, n, m )
//    // Modular exponentiation.
//    //
//    // Calling Sequence
//    //   x = number_powermod ( a, n, m )
//    //
//    // Parameters
//    //   a : a 1x1 matrix of floating point integers, must be positive
//    //   n : a 1x1 matrix of floating point integers, must be positive
//    //   m : a 1x1 matrix of floating point integers, must be positive
//    //   x : a 1x1 matrix of floating point integers
//    //
//    // Description
//    //    Returns x = a^n (mod m).
//    //
//    //    Uses a repeated squaring algorithm.
//    //    The algorithm may fail if the number is so large that the
//    //    square <literal>a^2</literal> is out of range.
//    //
//    //    Some programming tricks are used to speed up the computation, and to
//    //    allow computations in which A**N is much too large to store in a
//    //    real word.
//    //
//    //    First, for efficiency, the power A**N is computed by determining
//    //    the binary expansion of N, then computing A, A**2, A**4, and so on
//    //    by repeated squaring, and multiplying only those factors that
//    //    contribute to A**N.
//    //
//    //    Secondly, the intermediate products are immediately "mod'ed", which
//    //    keeps them small.
//    //
//    //    For instance, to compute mod ( A**13, 11 ), we essentially compute 13 = 1 + 4 + 8
//    //    then A**13 = A * A**4 * A**8. This leads to
//    //    mod ( A**13, 11 ) = mod ( A, 11 ) * mod ( A**4, 11 ) * mod ( A**8, 11 ).
//    //
//    //    Fermat's little theorem says that if P is prime, and A is not divisible
//    //    by P, then ( A**(P-1) - 1 ) is divisible by P.
//    //
//    // Authors
//    // Copyright (C) 2012 - Michael Baudin
//    // Copyright (C) 2009 - 2010 - DIGITEO - Michael Baudin
//    // Copyright (C) 2004 - John Burkardt
//    //
//
//    function x = powerModBasis(a,n,m)
//        // Assume n>=0, m>=0.
//        //
//        //  A contains the successive squares of A.
//        x = 1
//        while ( 0 < n )
//            d = pmodulo ( n, 2 )
//            if ( d == 1 )
//                x = number_asmodmint(a,x,m)
//            end
//            a = number_asmodmint(a,a,m)
//            n = floor ( ( n - d ) / 2 )
//        end
//        //
//        //  Ensure that 0 <= X.
//        while ( x < 0 )
//            x = x + m
//        end
//    endfunction
//    //
//    // Proceed...
//    if (m==0) then
//        x = 0
//        return
//    end
//    //
//    // 1. Pre-process the input
//    if (n>=0) then
//        invx = %f
//    else
//        // If n is negative, compute the inverse.
//        invx = %t
//        n = -n
//    end
//    if (m>=0) then
//        oppx = %f
//    else
//        // If m is negative, compute the opposite.
//        oppx = %t
//        m = -m
//    end
//    //
//    // 2. Compute with positive arguments
//    x = powerModBasis(a,n,m)
//    //
//    // 3. Post-process the output
//    if (invx) then
//        x = number_inversemod(x,m)
//    end
//    if (oppx) then
//        x = -x
//    end
//endfunction
// Copyright (C) 2012 - Michael Baudin
// Copyright (C) 2009 - 2010 - DIGITEO - Michael Baudin
// Copyright (C) 2004 - John Burkardt
//

// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function x = number_powermodint ( a, n, m )
    // Modular exponentiation.
    //
    // Calling Sequence
    //   x = number_powermod ( a, n, m )
    //
    // Parameters
    //   a : a 1x1 matrix of floating point integers, must be positive
    //   n : a 1x1 matrix of floating point integers, must be positive
    //   m : a 1x1 matrix of floating point integers, must be positive
    //   x : a 1x1 matrix of floating point integers
    //
    // Description
    //    Returns x = a^n (mod m).
    //
    //    Uses a repeated squaring algorithm.
    //    The algorithm may fail if the number is so large that the
    //    square <literal>a^2</literal> is out of range.
    //
    //    Some programming tricks are used to speed up the computation, and to
    //    allow computations in which A**N is much too large to store in a
    //    real word.
    //
    //    First, for efficiency, the power A**N is computed by determining
    //    the binary expansion of N, then computing A, A**2, A**4, and so on
    //    by repeated squaring, and multiplying only those factors that
    //    contribute to A**N.
    //
    //    Secondly, the intermediate products are immediately "mod'ed", which
    //    keeps them small.
    //
    //    For instance, to compute mod ( A**13, 11 ), we essentially compute 13 = 1 + 4 + 8
    //    then A**13 = A * A**4 * A**8. This leads to
    //    mod ( A**13, 11 ) = mod ( A, 11 ) * mod ( A**4, 11 ) * mod ( A**8, 11 ).
    //
    //    Fermat's little theorem says that if P is prime, and A is not divisible
    //    by P, then ( A**(P-1) - 1 ) is divisible by P.
    //
    // Authors
    // Copyright (C) 2012 - Michael Baudin
    // Copyright (C) 2009 - 2010 - DIGITEO - Michael Baudin
    // Copyright (C) 2004 - John Burkardt
    //

    function x = powerModBasis(a,n,m)
        // Assume n>=0, m>=0.
        //
        //  A contains the successive squares of A.
        x = 1
        while ( 0 < n )
            d = pmodulo ( n, 2 )
            if ( d == 1 )
                x = number_productmodint(a,x,m)
            end
            a = number_productmodint(a,a,m)
            n = floor ( ( n - d ) / 2 )
        end
        //
        //  Ensure that 0 <= X.
        while ( x < 0 )
            x = x + m
        end
    endfunction
    //
    // Proceed...
    if (m==0) then
        x = 0
        return
    end
    //
    // 1. Pre-process the input
    if (n>=0) then
        invx = %f
    else
        // If n is negative, compute the inverse.
        invx = %t
        n = -n
    end
    if (m>=0) then
        oppx = %f
    else
        // If m is negative, compute the opposite.
        oppx = %t
        m = -m
    end
    //
    // 2. Compute with positive arguments
    x = powerModBasis(a,n,m)
    //
    // 3. Post-process the output
    if (invx) then
        x = number_inversemod(x,m)
    end
    if (oppx) then
        x = -x
    end
endfunction
