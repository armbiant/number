// Copyright (C) 2009 - 2010 - DIGITEO - Michael Baudin

// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function a = number_fermat ( n )
  //   Returns the Fermat number 2^2^n + 1
  //
  // Calling Sequence
  //
  // Parameters
  // n : a 1x1 matrix of floating point integers, must be positive
  // a : a 1x1 matrix of floating point integers
  //
  // Description
  //   Returns the Fermat number :
  //
  //<latex>
  //\begin{eqnarray}
  //a = 2^{2^n} + 1
  //\end{eqnarray}
  //</latex>
  //
  // Examples
  // number_fermat ( 0 ) // 3
  // number_fermat ( 1 ) // 5
  // number_fermat ( 2 ) // 17
  // number_fermat ( 3 ) // 257
  // number_fermat ( 4 ) // 65537
  // number_fermat ( 5 ) // 641*6700417
  //
  // Authors
  // Copyright (C) 2009 - 2010 - DIGITEO - Michael Baudin
  //

  [lhs, rhs] = argn()
  apifun_checkrhs ( "number_fermat" , rhs , 1 )
  apifun_checklhs ( "number_fermat" , lhs , 0:1 )
  //
  // Check type
  apifun_checktype ( "number_fermat" , n , "n" , 1 , "constant" )
  //
  // Check size
  apifun_checkscalar ( "number_fermat" , n , "n" , 1 )
  //
  // Check content
  apifun_checkflint ( "number_fermat" , n , "n" , 1 )
  apifun_checkrange ( "number_fermat" , n , "n" , 1 , 0 , 2^53 )
  //
  a = 2^(2^n) + 1
endfunction

