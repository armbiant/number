// Copyright (C) 2009 - 2010 - DIGITEO - Michael Baudin

// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function isdivisor = number_isdivisor ( a , b )
  // Checks if a divides b.
  //
  // Calling Sequence
  // isdivisor = number_isdivisor ( a , b )
  //
  // Parameters
  // a : a 1x1 matrix of floating point integers, must be positive
  // b : a 1x1 matrix of floating point integers, must be positive
  // isdivisor : a 1x1 matrix of booleans
  //
  // Description
  //   Returns %t if a divides b.
  //   Returns %f if not.
  //
  // Examples
  // number_isdivisor ( 3 , 5 ) // %f
  // number_isdivisor ( 3 , 9 ) // %t
  //
  // Bibliography
  //   "Introduction to algorithms", Cormen, Leiserson, Rivest, Stein, 2nd edition
  //
  // Authors
  // Copyright (C) 2009 - 2010 - DIGITEO - Michael Baudin
  //

  [lhs, rhs] = argn()
  apifun_checkrhs ( "number_isdivisor" , rhs , 2 )
  apifun_checklhs ( "number_isdivisor" , lhs , 0:1 )
  //
  // Check type
  apifun_checktype ( "number_isdivisor" , a , "a" , 1 , "constant" )
  apifun_checktype ( "number_isdivisor" , b , "b" , 2 , "constant" )
  //
  // Check size
  apifun_checkscalar ( "number_isdivisor" , a , "a" , 1 )
  apifun_checkscalar ( "number_isdivisor" , b , "b" , 2 )
  //
  // Check content
  apifun_checkflint ( "number_isdivisor" , a , "a" , 1 )
  apifun_checkrange ( "number_isdivisor" , a , "a" , 1 , 0 , 2^53 )
  apifun_checkflint ( "number_isdivisor" , b , "b" , 2 )
  apifun_checkrange ( "number_isdivisor" , b , "b" , 2 , 0 , 2^53 )
  //
  // Proceed...
  if ( ( b / a ) - floor ( b / a ) == 0 ) then
    isdivisor = %t
  else
    isdivisor = %f
  end
endfunction

