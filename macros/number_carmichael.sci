// Copyright (C) 2009 - 2010 - DIGITEO - Michael Baudin

// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function a = number_carmichael ( )
  //   Returns some Carmichael numbers.
  //
  // Calling Sequence
  //   a = number_carmichael ( )
  //
  // Parameters
  //   a : a 33x1 matrix of floating point integers, some Carmichael numbers
  //
  // Description
  // In number theory, a Carmichael number is a composite positive integer n which
  // satisfies the congruence
  //
  //<latex>
  //\begin{eqnarray}
  //b^{n-1} \equiv 1\pmod{n}
  //\end{eqnarray}
  //</latex>
  //
  // for all integers b which are relatively prime to n.
  //
  // Examples
  //   a = number_carmichael ( )
  //
  // // Test Carmichael #1 in the range b=[1,50]
  // n=a(1);
  // bmin = 1;
  // bmax = 50;
  // for b = bmin : bmax
  //   if ( number_gcd(b,n) == 1 ) then
  //     x = number_powermod ( b, n-1, n );
  //     mprintf("gcd(%d,%d)=1 and %d^%d = %d (mod %d)\n",..
  //       b,n,b,n-1,x,n);
  //   end
  // end
  //
  // Bibliography
  //   http://www.research.att.com/~njas/sequences/table?a=2997&fmt=4
  //   Carmichael number, http://en.wikipedia.org/wiki/Carmichael_numbers
  //
  // Authors
  // Copyright (C) 2009 - 2010 - DIGITEO - Michael Baudin
  //

  a = [
    561
    1105
    1729
    2465
    2821
    6601
    8911
    10585
    15841
    29341
    41041
    46657
    52633
    62745
    63973
    75361
    101101
    115921
    126217
    162401
    172081
    188461
    252601
    278545
    294409
    314821
    334153
    340561
    399001
    410041
    449065
    488881
    512461
 ]
endfunction

