// Copyright (C) 2009 - 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function r = number_multiprimality ( n )
  // Returns the number of prime factors of n.
  //
  // Calling Sequence
  //   r = number_multiprimality ( n )
  //
  // Parameters
  //  n : a r-by-c matrix of floating point integers
  //  r : a r-by-c matrix of floating point integers
  //
  // Description
  //   Returns the number of prime factors, not necessarily distinct, of n.
  //
  // Examples
  // number_multiprimality ( 4 ) // 2
  //
  // // This function is vectorized
  // number_multiprimality ( 1:20 )
  // expected = [1, 1, 1, 2, 1, 2, 1, 3, 2, 2, 1, 3, 1, 2, 2, 4, 1, 3, 1, 3]
  //
  // // Plot it
  // scf();
  // n = 1:100;
  // r = number_multiprimality ( n );
  // plot(n,r)
  // xtitle("Multiprimality","n","Number of non-distinct prime factors")
  //
  // Bibliography
  // http://mathworld.wolfram.com/PrimeFactor.html
  //
  // Authors
  // Copyright (C) 2010 - DIGITEO - Michael Baudin
  //

  [lhs,rhs]=argn();
  apifun_checkrhs ( "number_multiprimality" , rhs , 1 )
  apifun_checklhs ( "number_multiprimality" , lhs , 0:1 )
  //
  // Check arguments
  //
  // Check type
  apifun_checktype ( "number_multiprimality" , n , "n" , 1 , "constant" )
  //
  // Check size
  // Nothing to check
  //
  // Check content
  // TODO : use apifun_checkflint
  apifun_checkflint ( "number_multiprimality" , n , "n" , 1 )
  apifun_checkrange ( "number_multiprimality" , n , "n" , 1 , -2^53 , 2^53 )
  //
  // Proceed...
  [nrows,ncols] = size(n)
  r = zeros(nrows,ncols)
  for i = 1:nrows
  for j = 1:ncols
    result = number_factor ( n(i,j) )
    r(i,j)=size(result,"*")
    end
  end
endfunction


