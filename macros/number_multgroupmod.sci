// Copyright (C) 2012 - Michael Baudin

// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function Z = number_multgroupmod ( m )
    // Returns the multiplicative group modulo m.
    //
    // Calling Sequence
    //   Z = number_multgroupmod ( m )
    //
    // Parameters
    //   m : a 1-by-1 matrix of floating point integers, must be greater or equal than 1.
    //   Z : a eulerphi-by-1 matrix of floating point integers, the multiplicative group modulo m.
    //
    // Description
    // The elements of the multiplicative group modulo m are the
    // integers a in the set {1,2,...,m} which are coprime
    // (i.e. relatively prime) to m.
    // That is, for every a in Z, the output of
    //
    //   <screen>
    // number_coprime ( a , m )
    //   </screen>
    //
    // is true.
    //
    // The number of rows in Z, i.e. the size of the multiplicative group,
    // is number_eulertotient(m).
    //
    // This is a group under multiplication,
    // that is, if a and b are in Z, therefore a*b (mod m) is in Z.
    //
    // Examples
    // // See the multiplicative group modulo 10
    // // 1.    3.    7.    9.
    // Z = number_multgroupmod(10)
    // // The number of entries in this group is
    // Z = number_eulertotient(10)
    // // See that this is a multiplicative group.
    // modulo(3*7,10)
    // modulo(3*9,10)
    // modulo(7*9,10)
    //
    // // See the multiplicative group modulo 14
    // // 1.    3.    5.    9.    11.    13.
    // Z = number_multgroupmod(14)
    // // Its size is phi(14)=6
    // number_eulertotient(14)
    //
    // Authors
    // Copyright (C) 2012 - Michael Baudin
    //

    [lhs, rhs] = argn()
    apifun_checkrhs ( "number_multgroupmod" , rhs , 1 )
    apifun_checklhs ( "number_multgroupmod" , lhs , 0:1 )
    //
    // Check type
    apifun_checktype ( "number_multgroupmod" , m , "m" , 1 , "constant" )
    //
    // Check size
    apifun_checkscalar ( "number_multgroupmod" , m , "m" , 1 )
    //
    // Check content
    apifun_checkflint ( "number_multgroupmod" , m , "m" , 1 )
    apifun_checkrange ( "number_multgroupmod" , m , "m" , 1 , 1 , 2^53 )
    //
    // Proceed...
    Z = []
    for a = 1 : m
        if ( number_coprime(a,m) ) then
            Z($+1)=a
        end
    end
endfunction

