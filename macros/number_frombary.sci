// Copyright (C) 2010 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function n = number_frombary ( varargin )
  // Compute a number from a matrix of digits.
  //
  // Calling Sequence
  //   n = number_frombary ( d )
  //   n = number_frombary ( d , basis )
  //   n = number_frombary ( d , basis , order )
  //
  // Parameters
  //   basis : a 1x1 matrix of floating point integers, the basis (default 2)
  //   d : a matrix of floating point integers, the digits
  //   order : a 1x1 matrix of strings, the order (default "littleendian")
  //   n : a 1x1 matrix of doubles
  //
  // Description
  //   Given a matrix of digits d(i), i=1,2,...,p,
  //   and a basis b, returns the "littleendian" number
  //   <programlisting>
  //   n = d(p) + d(p-1)*b + d(p-2)*b^2 + ... + d(1)*b^(p-1)
  //   </programlisting>
  //   If "bigendian" order is chosen, returns the number
  //   <programlisting>
  //   n = d(1) + d(2)*b + d(3)*b^2 + ... + d(p)*b^(p-1)
  //   </programlisting>
  //
  // Examples
  // n = number_frombary ( [1 0 0] ) // 4
  // basis = 2;
  // n = number_frombary ( [1 0 0] , basis ) // 4
  // n = number_frombary ( [1 0 0] , basis , "littleendian" ) // 4
  // n = number_frombary ( [0 0 1] , basis , "bigendian" ) // 4
  //
  // Authors
  // Copyright (C) 2010 - Michael Baudin
  //

  [lhs, rhs] = argn()
  apifun_checkrhs ( "number_frombary" , rhs , 1:3 )
  apifun_checklhs ( "number_frombary" , lhs , 1 )
  //
  d = varargin ( 1 )
  basis = apifun_argindefault (  varargin , 2 , 2 )
  order = apifun_argindefault (  varargin , 3 , "littleendian" )
  //
  apifun_checktype ( "number_frombary" , d , "d" , 1 , "constant" )
  apifun_checktype ( "number_frombary" , basis , "basis" , 2 , "constant" )
  apifun_checktype ( "number_frombary" , order , "order" , 3 , "string" )
  //
  apifun_checkscalar ( "number_frombary" , basis , "basis" , 2 )
  apifun_checkscalar ( "number_frombary" , order , "order" , 3 )
  //
  apifun_checkgreq ( "number_frombary" , d , "d" , 1 , 0 )
  apifun_checkgreq ( "number_frombary" , basis , "basis" , 2 , 2 )
  apifun_checkoption ( "number_frombary" , order , "order" , 3 , ["littleendian" "bigendian"] )
  //
  select order
  case "littleendian"
    n = 0
    p = size(d,"*")
    q = basis^(p-1)
    for i = 1 : p
      n = n + d(i) * q
      q = q / basis
    end
  case "bigendian"
    n = 0
    q = 1
    p = size(d,"*")
    for i = 1 : p
      n = n + d(i) * q
      q = q * basis
    end
  else
    error ( mprintf ( gettext ( "%s: Unknown order"  ), "number_tobary" ) )
  end
endfunction

