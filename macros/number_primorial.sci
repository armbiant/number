// Copyright (C) 2009 - 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function p = number_primorial ( n )
  // Returns the product of all primes lower or equal to n
  //
  // Calling Sequence
  // p = number_primorial ( n )
  //
  // Parameters
  // n : a 1x1 matrix of floating point integers, must be positive
  // p : a 1x1 matrix of floating point integers
  //
  // Description
  //   The primorial function p(n) is p1*p2*...*pn,
  //   where pi are primes lower or equal than n.
  //
  // Examples
  //  number_primorial ( 2 ) // 2
  // number_primorial ( 3 ) // 2*3
  // number_primorial ( 4 ) // 2*3
  // number_primorial ( 7 ) // 2*3*5*7
  //
  // Authors
  // Copyright (C) 2009 - 2010 - DIGITEO - Michael Baudin
  //

  [lhs, rhs] = argn()
  apifun_checkrhs ( "number_primorial" , rhs , 1 )
  apifun_checklhs ( "number_primorial" , lhs , 0:1 )
  //
  // Check type
  apifun_checktype ( "number_primorial" , n , "n" , 1 , "constant" )
  //
  // Check size
  apifun_checkscalar ( "number_primorial" , n , "n" , 1 )
  //
  // Check content
  apifun_checkflint ( "number_primorial" , n , "n" , 1 )
  apifun_checkrange ( "number_primorial" , n , "n" , 1 , 0 , 2^53 )
  //
  // Proceed...
  primesarray = number_primes ( n )
  p = prod ( primesarray )
endfunction

