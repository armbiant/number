// Copyright (C) 2012 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function G = number_primitiveroot ( varargin )
    // Find a primitive root modulo m.
    //
    // Calling Sequence
    //   G=number_primitiveroot(m)
    //   G=number_primitiveroot(m,"smallest")
    //   G=number_primitiveroot(m,imax)
    //   G=number_primitiveroot(m,imax,selectfun)
    //
    // Parameters
    //  m : a 1-by-1 matrix of floating point integers, the modulo
    //  imax : a 1-by-1 matrix of doubles or strings (default imax=1), the maximum number of primitive roots to compute.
    //  selectfun : a function or a list, a selection function (default selectfun=[], which means that all primitive roots are selected). See below for details.
    //  G : a sg_by-1 matrix floating point integers, the primitive roots modulo m.
    //
    // Description
    //   This function computes primitive roots modulo m.
    //   If there is no primitive root modulo m, returns
    //   the empty matrix [].
    //
    //   By default, we have imax==1, so that we search a primitive
    //   root modulo m.
    //   In this case, the computed primitive root is not
    //   necessarily the smallest.
    //   Moreover, the selectfun input argument is ignored if imax==1.
    //
    //   The calling sequence G=number_primitiveroot(m,"smallest")
    //   computes the smallest primitive root modulo m.
    //
    //   If imax>1, we stop the search when we have found imax primitive roots.
    //   In this case, the primitive roots are in increasing order and
    //   G(1) is the smallest possible primitive root.
    //   Moreover, only the primitive roots a which are so that selectfun(a)
    //   is true are kept in G.
    //
    //   If imax is infinite, we stop the search
    //   when all primitive roots have been found.
    //
    // The function selectfun should have header
    //
    // <screen>
    // tf = selectfun( a )
    // </screen>
    //
    // where
    // a is a floating point integer, representing a primitive root,
    // tf is a 1-by-1 matrix of booleans.
    // If tf is true, then the primitive root a is included in the matrix G.
    //
    // It might happen that the function requires additionnal
    // arguments to be evaluated.
    // In this case, we can use the following feature.
    // The argument selectfun can also be a list, where the first
    // item is a function f.
    // The function f must have the header
    //
    // <screen>
    // tf = f ( a , a1 , a2 , ... ).
    // </screen>
    //
    // In this case the selectfun variable should hold the list (f,a1,a2,...) and the
    // input arguments a1, a2, will be automatically be appended at the
    // end of the calling sequence of f.
    //
    // Definition of a primitive root
    //
    // Let a be an integer relatively prime to m.
    // If the order of a modulo m is number_eulertotient(m),
    // then a is a primitive root modulo m.
    // In other words, if
    //
    // <screen>
    // number_multorder(a,m)==number_eulertotient(m)
    // </screen>
    //
    // then a is a primitive root modulo m.
    //
    // If there exists a primitive root modulo m, then
    // the number of primitive roots modulo m is
    //
    // <screen>
    // number_eulertotient(number_eulertotient(m))
    // </screen>
    //
    // Notes on the performances of the algorithm
    //
    // The fastest performance is achieved with imax==1 and selectfun is empty (i.e.
    // with the default settings).
    // This is because the returned primitive root does not have
    // to be the smallest.
    //
    // The calling sequence G=number_primitiveroot(m,"smallest")
    // requires that the primitive root is the smallest.
    // This implies that all candidates a=1,2,... have to be
    // tried before a primitive root can be found.
    // For small values of m, this is fast.
    // But for large values of m, this may require
    // a large number of iterations.
    //
    // Examples
    // // Check that a=5 is the only primitive root modulo 6.
    // G = number_primitiveroot ( 6 )
    // G = number_primitiveroot ( 6 , %inf )
    //
    // // Find all the primitive roots modulo 11
    // G = number_primitiveroot ( 11 , %inf )
    // expected = [2 6 7 8]'
    // // There is more that on primitive root.
    // // Therefore, the number of primitive roots is: 4
    // number_eulertotient(number_eulertotient(11))
    //
    // // There is no primitive root modulo 24
    // G = number_primitiveroot ( 24 , %inf )
    //
    // // Find one primitive root modulo 218: 115
    // G = number_primitiveroot ( 218 )
    // // Find the smallest primitive root modulo 218: 11
    // G = number_primitiveroot ( 218 , "smallest" )
    //
    // // Find one primitive root modulo 223 : 3
    // G = number_primitiveroot ( 223 )
    //
    // // Print the primitive roots modulo m
    // // for m=1,2,...,15
    // for m = 1 : 15
    //     X = number_primitiveroot(m,%inf);
    //     mprintf("Primitive roots mod %d: %s\n",..
    //         m,strcat(string(X),","));
    // end
    //
    // // Print the smallest primitive roots modulo m
    // // for m=1,2,...,227, if any
    // for m = 1 : 50
    //     g = number_primitiveroot(m,"smallest");
    //     if ( g<>[] ) then
    //         mprintf("Primitive root mod %d: %d\n",..
    //         m,g);
    //     end
    // end
    //
    // // Find the smallest prime primitive root modulo 3545281
    // // This is 163
    // function tf = selectprime(a)
    //     tf = number_isprime(a)
    // endfunction
    // number_primitiveroot(3545281,"smallest",selectprime)
    //
    // Bibliography
    // http://en.wikipedia.org/wiki/Primitive_root_modulo_n
    // http://people.cs.kuleuven.be/~dirk.nuyens/code/generatorp.m
    // An Introduction to the Theory of Numbers, 5th Edition, Ivan Niven, Herbert S. Zuckerman, Hugh L. Montgomery, 1991, John Wiley & Sons
    // The primitive root theorem, Amin Witno, 2012, WON Series in Discrete Mathematics and Modern Algebra Volume 5
    //
    // Authors
    // Copyright (C) 2012 - Michael Baudin
    // Copyright (C) 2004 - Dirk Nuyens

    //
    // Load Internals lib
    path = get_function_path("number_primitiveroot")
    path = fullpath(fullfile(fileparts(path)))
    numberinternalslib = lib(fullfile(path,"internals"));

    //
    // Define a default selection function
    function tf =__selectfun_default__(a)
        tf = %t
    endfunction

    [lhs,rhs]=argn();
    apifun_checkrhs ( "number_primitiveroot" , rhs , 1:3 )
    apifun_checklhs ( "number_primitiveroot" , lhs , 1 )
    //
    // Get arguments
    m = varargin ( 1 )
    imax = apifun_argindefault (  varargin , 2 , 1 )
    __selectfun__ = apifun_argindefault (  varargin , 3 , [] )
    //
    // Check arguments
    //
    // Check type
    apifun_checktype ( "number_primitiveroot" , m , "m" , 1 , "constant" )
    apifun_checktype ( "number_primitiveroot" , imax , "imax" , 2 , ["constant" "string"] )
    if ( __selectfun__ <> [] ) then
        apifun_checktype ( "number_primitiveroot" , __selectfun__ , "selectfun" , 3 , ["function" "list"] )
    end
    //
    // Check size
    apifun_checkscalar ( "number_primitiveroot" , m , "m" , 1 )
    apifun_checkscalar ( "number_primitiveroot" , imax , "imax" , 2 )
    //
    // Check content
    apifun_checkflint ( "number_primitiveroot" , m , "m" , 1 )
    apifun_checkrange ( "number_primitiveroot" , m , "m" , 1 , 0 , 2^53 )
    if (typeof(imax)=="constant") then
        if (imax <> %inf ) then
            apifun_checkflint ( "number_primitiveroot" , imax , "imax" , 2 )
            apifun_checkrange ( "number_primitiveroot" , imax , "imax" , 2 , 1 , 2^53 )
        end
    else
        apifun_checkoption( "number_primitiveroot" , imax , "imax" , 2 , "smallest" )
    end
    //
    // Proceed...
    G = number_searchDriver ( m , imax , __selectfun__ )
endfunction

function G = number_searchDriver ( m , imax , __selectfun__ )
    // This is the algorithm for a general m.
    // It searches the primitive roots modulo m.

    //
    // See if there exists a primitive root.
    tf = number_hasoneprimroot(m)
    if (~tf) then
        G = []
        return
    end
    //
    // Special cases
    if (m==2) then
        G = 1
        return
    elseif (m==4) then
        G = 3
        return
    end
    //
    // Search in the general case.
    if ( imax==1 & __selectfun__==[] ) then
        // Search one primitive root.
        // Any primitive root will do the job : use the
        // fastest algorithm.
        G = number_searchOne(m)
        return
    end
    if (__selectfun__==[]) then
        __selectfun__ = __selectfun_default__
    end
    if ( imax=="smallest" ) then
        // Search the smallest primitive root
        G = number_searchSmallest ( m , __selectfun__ )
        return
    end
    // Search imax primitive roots
    a = number_searchOne(m)
    G = number_genallprimroots(a,m, imax , __selectfun__ )
endfunction

function a = number_searchSmallest ( m , __selectfun__ )
    //  Searches the smallest primitive root modulo m.
    //
    // Parameters
    //  m : a 1-by-1 matrix of floating point integers, the modulo
    //  a : a sg_by-1 matrix floating point integers, the primitive roots modulo m.
    //
    // Description
    // This algorithm searches for the smallest primitive root
    // by performing a trial-and-error search for increasing values of
    // the primitive root, i.e. for a=2,3,...

    //
    // Initialization
    selectfuntype = typeof(__selectfun__)
    if ( selectfuntype == "list" ) then
        // List
        __selectfun__f = __selectfun__(1);
        __selectfun__args = __selectfun__(2:$)
    else
        __selectfun__f = __selectfun__
        __selectfun__args = list()
    end
    isprime = number_isprime ( m )
    if ( isprime ) then
        phi = m-1
    else
        phi = number_eulertotient(m)
    end
    factorsphi = number_factor ( phi )
    factorsphi = unique(factorsphi)
    for a = 2 : m-1
        //
        // 1. Check that a^phi == 1 (mod m)
        // If m is prime, then gcd(a,m)=1, which implies
        // a^phi == 1 (mod m) by Euler's theorem.
        if ( ~isprime ) then
            r = number_powermodint ( a, phi, m )
            if (r<>1) then
                // Go on to the next candidate a
                continue
            end
        end
        //
        // 2. Check that a^(phi/p) == 1 (mod m)
        //    for all prime factors of phi
        isprimeroot = %t
        for pk = factorsphi
            f = int(phi/pk)
            r = number_powermodint ( a, f, m )
            if (r==1) then
                // Go on to the next candidate a
                isprimeroot = %f
                break
            end
        end
        if ( isprimeroot ) then
            //
            // See if the selection function keeps the primitive root.
            tf = __selectfun__f ( a , __selectfun__args(:))
            if ( tf ) then
                // We have found the smallest !
                break
            end
        end
    end
endfunction
function g = number_searchOne(m)
    //  Searches one single primitive root modulo m.
    //
    // Description
    //  The computed primitive root is not necessarilly the smallest.
    //  m=2 or m=4 or m of the form m=p^k or m=2*p^k, with p an odd prime.
    //  If m is not of the above form, the multiplicative group modulo m has no
    //  primitive root.
    //
    //  (C) 2004 <dirk.nuyens@cs.kuleuven.ac.be>

    [uf,nf] = number_getprimefactors ( m )
    nbfac = size(uf,"*")
    if ((nbfac == 2) & (uf(1) == 2) & ...
        (nf(1) == 1) & (modulo(uf(2), 2) == 1) ) then
        //  m is of the form 2 p^k, with p an odd prime
        p = uf(2);
        k = nf(2);
        g = number_searchSmallest(p,__selectfun_default__)
        if (modulo(g, 2) == 0) then
            g = g + p^k
        end
    elseif ( (nbfac == 1) & (modulo(uf(1), 2) == 1) ) then
        //  m is of the form p^k, with p an odd prime
        p = uf(1)
        k = nf(1)
        g = number_searchSmallest(p,__selectfun_default__)
        if (k > 1) then
            t = number_powermod(g, p-1, p^2)
            if (t == 1) then
                //  This happens first for the prime 40487,
                //  that is for m = 40487^2 = 1639197169.
                //  The number_powermod routine will then incorrectly
                //   calculate 5^{40487-1} (mod 1639197169) unless using
                //  the arbitrary precision integers from Java.
                g = g + p
            end
        end
    elseif (m == 2) then
        g = 1
    elseif (m == 4) then
        g = 3
    else
        g = []
    end
endfunction
function tf = number_hasoneprimroot(m)
    //  Returns %t if there exists a primitive root modulo m.
    //
    // Description
    //  We know that there is a primitive root modulo m if
    //  m=2 or m=4 or m of the form m=p^k or m=2*p^k, with p an odd prime.
    //  If m is not of the above form, the multiplicative group modulo m has no
    //  primitive root.
    //  (C) 2004 <dirk.nuyens@cs.kuleuven.ac.be>

    if (m==0) then
        tf = %f
        return
    elseif (m==1) then
        tf = %f
        return
    end

    [uf,nf] = number_getprimefactors ( m )
    nbfac = size(uf,"*")
    if ((nbfac == 2) & (uf(1) == 2) & ...
        (nf(1) == 1) & (modulo(uf(2), 2) == 1) ) then
        //  m is of the form 2 p^k, with p an odd prime
        tf = %t
    elseif ( (nbfac == 1) & (modulo(uf(1), 2) == 1) ) then
        //  m is of the form p^k, with p an odd prime
        tf = %t
    elseif (m == 2) then
        tf = %t
    elseif (m == 4) then
        tf = %t
    else
        tf = %f
    end
endfunction

function G = number_genallprimroots(a,m, imax , __selectfun__ )
    // Compute all primitive roots modulo m,
    // given one primitive root modulo m.
    //
    // Parameters
    //  a : a 1-by-1 matrix of floating point integers, a primitive root
    //  m : a 1-by-1 matrix of floating point integers, the modulo
    //  imax : a 1-by-1 matrix of floating point integers, the maximum number of primitive roots to search for (default imax=1).
    //  selectfun : a function or a list, a selection function (default selectfun=[], which means that all primitive roots are selected). See below for details.
    //  G : a sg_by-1 matrix floating point integers, the primitive roots modulo m, sorted in increasing order.
    //
    // Description
    // This function uses the properties that some powers a^k
    // of a given primitive root a modulo m are primitive roots modulo m.
    // But not all powers: there are conditions on k.
    //
    // Theorem. Let a be a primitive root modulo m.
    // Then a^k is also a primitive root modulo m if and only
    // if k is relatively prime to phi(m).
    //
    // There is a corollary to this theorem.
    //
    // Corollary. If any exists, there are exactly phi(phi(n))
    // primitive roots modulo n.
    //
    // On output, the primitive roots are in increasing order.
    //
    // We stop when imax primitive roots have been found.
    //
    // The selection function __selectfun__ returns
    // true only when the primitive root is to be kept.
    // Otherwise, the algorithm considers that this is not
    // a primitive root to consider, and move on to the
    // next candidate value of a.
    //
    // Bibliography
    // The primitive root theorem, Amin Witno, 2012, WON Series in Discrete Mathematics and Modern Algebra Volume 5

    selectfuntype = typeof(__selectfun__)
    if ( selectfuntype == "list" ) then
        // List
        __selectfun__f = __selectfun__(1);
        __selectfun__args = __selectfun__(2:$)
    else
        __selectfun__f = __selectfun__
        __selectfun__args = list()
    end

    //
    // Initialization
    tf = __selectfun__f ( a , __selectfun__args(:))
    if ( tf ) then
        G(1) = a
    else
        G = []
    end
    nfound = 0
    euphi = number_eulertotient(m)
    for k = 2:euphi-1
        if (number_gcdeuclidint(k,euphi)==1) then
            ak = number_powermodint(a,k,m);
            //
            // See if the selection function keeps the primitive root.
            tf = __selectfun__f ( ak , __selectfun__args(:))
            if ( tf ) then
                //
                // Store the primitive root
                nfound = nfound + 1
                G($+1) = ak
            end
            if ( nfound>=imax ) then
                break
            end
        end
    end
    //
    // Order the roots
    G = gsort(G,"g","i")
endfunction
