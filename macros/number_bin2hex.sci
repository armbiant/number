// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function hexstr = number_bin2hex ( binstr )
  // Converts a binary string into a hexadecimal string.
  //
  // Calling Sequence
  //   binstr = number_bin2hex ( hexstr )
  //
  // Parameters
  //   binstr : a matrix of strings. Each entry of the matrix must have length divisible by 4.
  //   hexstr : a matrix of strings.
  //
  // Description
  //   Returns the binary string corresponding to the given hexadecimal string.
  //   Returns upper case letter. Use convstr to change this to lower case.
  //
  //   The user may pad the binary string with additionnal "0" if necessary.
  //
  // Examples
  // binstr = "0011111111100101010010110011010100000100110001101011010010100011"
  // expected = "3FE54B3504C6B4A3";
  // hexstr = number_bin2hex (binstr)
  //
  // Authors
  // Copyright (C) 2010 - DIGITEO - Michael Baudin
  //

  [lhs, rhs] = argn()
  apifun_checkrhs ( "number_bin2hex" , rhs , 1 )
  apifun_checklhs ( "number_bin2hex" , lhs , 1 )
  //
  apifun_checktype ( "number_bin2hex" , binstr , "binstr" , 1 , "string" )
  //
  hexmap = [
     "0000" "0"
     "0001" "1"
     "0010" "2"
     "0011" "3"
     "0100" "4"
     "0101" "5"
     "0110" "6"
     "0111" "7"
     "1000" "8"
     "1001" "9"
     "1010" "A"
     "1011" "B"
     "1100" "C"
     "1101" "D"
     "1110" "E"
     "1111" "F"
  ]
  for ir = 1 : size(binstr,"r")
  for ic = 1 : size(binstr,"c")
    binlen = length(binstr(ir,ic))
    if ( floor(binlen/4) <> binlen/4 ) then
      error(msprintf(gettext("%s: Wrong length for entry (%d,%d) in input argument #%d: the length is %d, which cannot be divided by 4.\n"),"number_bin2hex" , ir,ic, 1, binlen ));
    end
    hexstr(ir,ic)=""
    for k = 1 : length(binstr(ir,ic))/4
      binchar = part(binstr,4*(k-1)+1:4*k)
      hexind = find(hexmap(:,1)==binchar)
      if ( hexind==[] ) then
        error(msprintf(gettext("%s: Letter %s at entry #%d at indices (%d,%d) in the matrix is not binary."),"number_bin2hex",binchar,k,ir,ic))
      end
      hexchar = hexmap(hexind,2)
      hexstr(ir,ic) = hexstr(ir,ic) + hexchar
    end
  end
  end
endfunction

