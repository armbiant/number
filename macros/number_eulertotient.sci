// Copyright (C) 2010 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function p = number_eulertotient(n)
    // Returns Euler totient function
    //
    // Calling Sequence
    //   p = number_eulertotient(n)
    //
    // Parameters
    //   n : a 1-by-1 matrix of doubles, integer value.
    //   p : a 1-by-1 matrix of doubles, integer value, Euler's totient function.
    //
    // Description
    // This function returns the number of positive integers 
    // less than or equal to n that are relatively prime to n. 
    // That is, if n is a positive integer, then eulertotient(n) is the 
    // number of integers k in the range 1 = k = n for which gcd(n, k) = 1.
    //
    // Euler's totient function is equal to
	//
    //<latex>
    //\begin{eqnarray}
    //\varphi(n) =n \prod_{p\mid n} \left(1-\frac{1}{p}\right)
    //\end{eqnarray}
    //</latex>
    //
    // Examples
    //   p = number_eulertotient(14)
    //
    // for n = 1 : 100
    // p(n) = number_eulertotient(n);
    // end
    // scf();
    // plot(1:100,p,"bo");
    // xtitle("Euler''s totient function","n","phi");
    //
    // Bibliography
    // http://en.wikipedia.org/wiki/Euler's_totient_function
    //
    // Author
    // Copyright (C) 2012 - Michael Baudin

  [lhs, rhs] = argn()
  apifun_checkrhs ( "number_eulertotient" , rhs , 1:1 )
  apifun_checklhs ( "number_eulertotient" , lhs , 0:1 )
  //
  // Check type
  apifun_checktype ( "number_eulertotient" , n , "n" , 1 , "constant" )
  //
  // Check size
  apifun_checkscalar ( "number_eulertotient" , n , "n" , 1 )
  //
  // Check content
  apifun_checkflint ( "number_eulertotient" , n , "n" , 1 )
  apifun_checkrange ( "number_eulertotient" , n , "n" , 1 , 0 , 2^53 )
  //
  // Proceed...
    if (n==1) then
        p = 1
        return
    end
    f = number_factor ( n )
    if ( size(f,"*")== 1 ) then
        // n is prime
        p = n-1
        return
    end
    f = unique(f)
    p = n*prod(1-1 ./f)
    p = int(p)
endfunction
