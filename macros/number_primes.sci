// Copyright (C) 2009 - 2010 - DIGITEO - Michael Baudin
// Copyright (C) INRIA - Farid BELAHCENE
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function result = number_primes ( varargin )
  // Computes the list of all primes up to n.
  //
  // Calling Sequence
  //   result = number_primes ( n )
  //   result = number_primes ( n , method )
  //
  // Parameters
  // n : a 1x1 matrix of floating point integers, must be positive, the maximum integer.
  // method : a 1x1 matrix of strings, the algorithm to use.
  // result : a nx1 matrix of floating point integers, the primes
  //
  // Description
  //   Returns the list of all primes from 1 up to n,
  //   as a row matrix.
  //
  //   The list of algorithms is the following.
  //    <variablelist>
  //                        <varlistentry>
  //                          <term> "erathostenefast" </term>
  //                          <listitem>
  //            <para>
  //            Uses a fast Erathostene's Sieve.
  //            This implementation is due to Farid BELAHCENE.
  //            It requires a lot of memory and may fail if n is too large.
  //            </para>
  //            </listitem>
  //                        </varlistentry>
  //                        <varlistentry>
  //                          <term> "erathostenenaive" </term>
  //                          <listitem>
  //            <para>
  //            Uses a naive Erathostene's Sieve.
  //            It requires a lot of memory and may fail if n is too large.
  //            </para>
  //            </listitem>
  //                        </varlistentry>
  //    </variablelist>
  //
  // Examples
  // expected = [2 3 5 7 11 13 17 19 23 29 31 37 41 43 47 ...
  //   53 59 61 67 71 73 79 83 89 97];
  // computed = number_primes ( 100 )
  // computed = number_primes ( 100 , "erathostenefast" )
  // computed = number_primes ( 100 , "erathostenenaive" )
  //
  // Authors
  // Copyright (C) 2009 - 2010 - DIGITEO - Michael Baudin
  // Copyright (C) INRIA - Farid BELAHCENE
  //

  [lhs, rhs] = argn()
  apifun_checkrhs ( "number_primes" , rhs , 1:2 )
  apifun_checklhs ( "number_primes" , lhs , 0:1 )
  //
  // Get arguments
  n = varargin ( 1 )
  method = apifun_argindefault (  varargin , 2 , "erathostenefast" )
  //
  // Check type
  apifun_checktype ( "number_primes" , n , "n" , 1 , "constant" )
  apifun_checktype ( "number_primes" , method , "method" , 2 , "string" )
  //
  // Check size
  apifun_checkscalar ( "number_primes" , n , "n" , 1 )
  apifun_checkscalar ( "number_primes" , method , "method" , 2 )
  //
  // Check content
  apifun_checkflint ( "number_primes" , n , "n" , 1 )
  apifun_checkrange ( "number_primes" , n , "n" , 1 , 0 , 2^53 )
  apifun_checkoption ( "number_primes" , method , "method" , 2 , ["erathostenefast" "erathostenenaive"] )
  //
  // Proceed...
  if ( n == 0 ) then
    result = [];
    return
  end
  select method
  case "erathostenefast"
    result = erathostenefast ( n )
  case "erathostenenaive"
    result = erathostenenaive ( n )
  else
    errmsg = sprintf ( gettext ( "%s: Unknown method %s" ) , "number_primes" , method )
    error(errmsg)
  end
endfunction
//
// erathostenefast --
//   Returns the list of all primes from 1 up to n.
//   Uses a fast algorithm, based on vectorized Scilab.
//
function y = erathostenefast ( x )
    y = (1:fix(x));
    ylength = size(y,'*')
    i = 2
    while (i <= sqrt(x))
        if y(i)<> 0
            y(2*y(i):y(i):ylength) = 0
        end
        i = i+1
    end
    //y(y<=1)=[]; too slow on scilab 6 for now
    y = y(y>1);
endfunction
//
// erathostenenaive --
//   Returns the list of all primes from 1 up to n.
//   Uses a straitforward algorithm.
//
function result = erathostenenaive ( n )
    sieve(1) = %f
    for i=2:n
      sieve(i) = %t
    end
    p = 2
    while ( p * p <= n )
      i = p * p
      while ( i<=n )
        sieve(i) = %f
        i = i + p
      end
      p = p + 1
    end
    result = []
    for i = 1:n
      if ( sieve(i) ) then
        result(1,$+1) = i
      end
    end
endfunction

