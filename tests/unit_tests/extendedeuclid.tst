// Copyright (C) 2008 - INRIA - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


[ d , x , y ] = number_extendedeuclid ( 99 , 78 );
assert_checkequal ( [ d , x , y ] , [3 -11 14] );

[ d , x , y ] = number_extendedeuclid ( 120 , 23 );
assert_checkequal ( [ d , x , y ] , [1 -9 47] );

