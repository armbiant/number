// Copyright (C) 2008-2009 - INRIA - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- CLI SHELL MODE -->


// Check for actual primes
assert_checkequal ( number_pseudoprime ( 7 ) , %t );
assert_checkequal ( number_pseudoprime ( 5 ) , %t );
assert_checkequal ( number_pseudoprime ( 5 , 3 ) , %t );
// Check for composite numbers
assert_checkequal ( number_pseudoprime ( 10 ) , %f );
assert_checkequal ( number_pseudoprime ( 20 ) , %f );
// Check for pseudo-primes : this is a known limitation of the test
// 341 = 11 * 31
assert_checkequal ( number_pseudoprime ( 341 ) , %t );
assert_checkequal ( number_pseudoprime ( 561 ) , %t );
assert_checkequal ( number_pseudoprime ( 645 ) , %t );
assert_checkequal ( number_pseudoprime ( 1105 ) , %t );

