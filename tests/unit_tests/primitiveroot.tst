// Copyright (C) 2012 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


G = number_primitiveroot ( 0 );
assert_checkequal(G,[]);
//
G = number_primitiveroot ( 1 );
assert_checkequal(G,[]);
//
G = number_primitiveroot ( 2 );
assert_checkequal(G,1);
//
G = number_primitiveroot ( 3 );
assert_checkequal(G,2);
//
G = number_primitiveroot ( 4 );
assert_checkequal(G,3);
//
// Check that a=5 is the only primitive root modulo 6.
G = number_primitiveroot ( 6 );
assert_checkequal(G,5);

// Find all the primitive roots modulo 11
G = number_primitiveroot ( 11 , %inf );
expected = [2 6 7 8]';
assert_checkequal(G,expected);

// Find all the primitive roots modulo 13
G = number_primitiveroot ( 13 , %inf );
expected = [2 6 7 11]';
assert_checkequal(G,expected);

// Find one primitive roots modulo 2
G = number_primitiveroot ( 2 );
assert_checkequal(G,1);

// There is no primitive root modulo 8
G = number_primitiveroot ( 8 );
assert_checkequal(G,[]);

function checkPrimitiveRoot(m)
    //
	// Check number_primitiveroot for this particular value of m.
	
    mprintf("Checking m=%d\n",m)
	//
	// Compute all primitive roots
    G = number_primitiveroot(m,%inf);
    if ( G<>[] ) then
		//
		// Check that the number of primitive roots is correct.
        nbroots = size(G,"*");
        expected = number_eulertotient(number_eulertotient(m));
        assert_checkequal(expected,nbroots);
		//
		// Check that each entry is a primitive root
		for i = 1 : nbroots
			a = G(i);
			tf = number_isprimroot(a,m);
			assert_checktrue(tf);
		end
    end
	//
	// Compute one primitive root
    G2 = number_primitiveroot(m);
	if ( G2==[] ) then
		assert_checktrue(G==[]);
	else
		assert_checkequal(size(G2,"*"),1)
		tf = number_isprimroot(G2,m);
		assert_checktrue(tf);
		// Check that G2 was found in G
		k = find(G2==G);
		assert_checktrue(k<>[]);
	end
	//
	// Compute the smallest primitive root
    G3 = number_primitiveroot(m,"smallest");
	if ( G3==[] ) then
		assert_checktrue(G==[]);
	else
		tf = number_isprimroot(G3,m);
		assert_checktrue(tf);
		// Check that G3 was found in G
		k = find(G3==G);
		assert_checktrue(k<>[]);
	end
endfunction


// Check number_primitiveroot when we compute 
// all roots.
for m = 1 : 20
	checkPrimitiveRoot(m)
end

// Smallest primitive root a modulo m
// [m a]
S = [
2    1    
3    2    
4    3    
5    2    
6    5    
7    3    
9    2    
10    3    
11    2    
13    2    
14    3    
17    3    
18    5    
19    2    
22    7    
23    5    
25    2    
26    7    
27    2    
29    2    
31    3    
34    3    
37    2    
38    3    
41    6    
43    3    
46    5    
47    5    
49    3    
50    3    
53    2    
54    5    
58    3    
59    2    
61    2    
62    3    
67    2    
71    7    
73    5    
74    5    
79    3    
81    2    
82    7    
83    2    
86    3    
89    3    
94    5    
97    5    
98    3    
101    2    
103    5    
106    3    
107    2    
109    6    
113    3    
118    11    
121    2    
122    7    
125    2    
127    3    
131    2    
134    7    
137    3    
139    2    
142    7    
146    5    
149    2    
151    6    
157    5    
158    3
162    5
163    2
166    5
167    5
169    2
173    2
178    3
179    2
181    2
191    19
193    5
194    5
197    2
199    3
202    3
206    5
211    2
214    5
218    11
223    3
226    3
227    2
];
	mprintf("Smallest primitive root\n\")
for i = 1 : size(S,"r")
    //
	// Check the smallest
    m = S(i,1);
	mprintf("Checking m=%d\n",m)
    a = S(i,2);
    G = number_primitiveroot(m,"smallest");
	assert_checkequal(G,a);
end

// Reference
// "Comments on search procedures for primitive roots"
// Eric Bach, Mathematics of Computation
// Volume 66, Number 220, October 1997, Pages 1719-1727
// Table of the smallest *prime* primitive root modulo a prime
// [m a]
S = [
3 2
7 3
23 5
41 7
109 11
191 19
271 43
2791 53
11971 79
31771 107
190321 149
2080597 151
3545281 163
4022911 211
73189117 223
137568061 263
443571241 277
565822531 307
1160260711 347
1622723341 349
];
// Select only prime primitive roots
function tf = selectprime(a)
    tf = number_isprime(a)
endfunction

mprintf("Smallest prime primitive root\n")
for i = 1 : 12
    m = S(i,1);
	mprintf("Checking m=%d\n",m)
    a = S(i,2);
    computed = number_primitiveroot(m,"smallest",selectprime);
    assert_checkequal(computed,a);
end
