// Copyright (C) 2008-2009 - INRIA - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// <-- CLI SHELL MODE -->
// Check for actual primes
assert_checkequal ( number_probableprime ( 7 ) , %t );
assert_checkequal ( number_probableprime ( 5 ) , %t );
assert_checkequal ( number_probableprime ( 5 , 3 ) , %t );
// Check for composite numbers
assert_checkequal ( number_probableprime ( 10 ) , %f );
assert_checkequal ( number_probableprime ( 20 ) , %f );
// Check for pseudo-primes : while pseudo prime does not detect that these numbers
// are composite, probable prime does it perfectly.
assert_checkequal ( number_probableprime ( 341 ) , %f );
assert_checkequal ( number_probableprime ( 561 ) , %f );
assert_checkequal ( number_probableprime ( 645 ) , %f );
assert_checkequal ( number_probableprime ( 1105 ) , %f );
// Test verbose
number_probableprime ( 10001 , [] , %t );
Loop 1/50
Found witness a = 6068
// Test a few numbers
mprintf("Test a few numbers\n");
Test a few numbers
integerlist  = [1  2  3  4  5  6  7  8  9  10 73 32003];
expectedlist = [%f %t %t %f %t %f %t %f %f %f %t %t].';
computed = [];
for i=1:size(integerlist,"*")
    mprintf ( "i=%d\n", integerlist ( i ) )
    computed = number_probableprime ( integerlist ( i ) );
    expected = expectedlist ( i );
    assert_checkequal ( computed , expected );
end
i=1
i=2
i=3
i=4
i=5
i=6
i=7
i=8
i=9
i=10
i=73
i=32003
// Test some even integers
mprintf("Test some even integers\n");
Test some even integers
integerlist = 2 * ( 2 : 50 );
expected = %f;
for i=1:size(integerlist,"*")
    mprintf ( "i=%d\n", integerlist ( i ) )
    computed = number_probableprime ( integerlist ( i ) );
    assert_checkequal ( computed , expected );
end
i=4
i=6
i=8
i=10
i=12
i=14
i=16
i=18
i=20
i=22
i=24
i=26
i=28
i=30
i=32
i=34
i=36
i=38
i=40
i=42
i=44
i=46
i=48
i=50
i=52
i=54
i=56
i=58
i=60
i=62
i=64
i=66
i=68
i=70
i=72
i=74
i=76
i=78
i=80
i=82
i=84
i=86
i=88
i=90
i=92
i=94
i=96
i=98
i=100
// Test some primes
mprintf("Test some primes\n");
Test some primes
integerlist = [2,5,73,109,113,293,557,563,593,601,823,947,967];
expected = %t;
for i=1:size(integerlist,"*")
    mprintf ( "i=%d\n", integerlist ( i ) )
    computed = number_probableprime ( integerlist ( i ) );
    assert_checkequal ( computed , expected );
end
i=2
i=5
i=73
i=109
i=113
i=293
i=557
i=563
i=593
i=601
i=823
i=947
i=967
// Check a large prime : too large for us...
//assert_checkequal ( number_probableprime ( 4432676798593 ) , %f );
