// Copyright (C) 2008 - INRIA - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


//
// Test the bary function
//
n = number_frombary ( [1 0 0] );
assert_checkequal ( n , 4 );
//
basis = 2;
n = number_frombary ( [1 0 0] , basis );
assert_checkequal ( n , 4 );
//
n = number_frombary ( [1 0 0] , basis , "littleendian" );
assert_checkequal ( n , 4 );
//
n = number_frombary ( [0 0 1] , basis , "bigendian" );
assert_checkequal ( n , 4 );
//
assert_checkequal ( number_frombary ( [] ) , 0 );

