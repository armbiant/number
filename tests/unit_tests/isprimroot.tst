// Copyright (C) 2012 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// Let's see that a=2 is a primitive root modulo m=13
tf = number_isprimroot(2,13);
assert_checktrue(tf);

// Let's see that a=3 is a primitive root modulo m=7
tf = number_isprimroot(3,7);
assert_checktrue(tf);

// Let's see that a=2 is a primitive root modulo m=9
tf = number_isprimroot(2,9);
assert_checktrue(tf);

// Let's see that a=5 is a primitive root modulo m=6
tf = number_isprimroot(5,6);
assert_checktrue(tf);

// Let's see that a=2 is NOT a primitive root modulo m=6
tf = number_isprimroot(2,6);
assert_checkfalse(tf);

// 3 is a primitive root modulo 49
tf = number_isprimroot(3,49);
assert_checktrue(tf);

// Check that a=5 is the only primitive root modulo 6.
m = 6;
expected = 5;
computed = [];
tf = [];
for a = 1 : 5
    tf(a) = number_isprimroot(a,m);
	if (tf(a)) then
	computed($+1)=a;
	end
end
assert_checkequal(computed,expected);

// Find all the primitive roots modulo 11
m = 11;
expected = [2 6 7 8]';
computed = [];
tf = [];
for a = 1 : m-1
    tf(a) = number_isprimroot(a,m);
	if (tf(a)) then
	computed($+1)=a;
	end
end
assert_checkequal(computed,expected);

// Find all the primitive roots modulo 13
m = 13;
expected = [2 6 7 11]';
computed = [];
tf = [];
for a = 1 : m-1
    tf(a) = number_isprimroot(a,m);
	if (tf(a)) then
	computed($+1)=a;
	end
end
assert_checkequal(computed,expected);



// Smallest primitive root a modulo m
// [m a]
S = [
2    1    
3    2    
4    3    
5    2    
6    5    
7    3    
9    2    
10    3    
11    2    
13    2    
14    3    
17    3    
18    5    
19    2    
22    7    
23    5    
25    2    
26    7    
27    2    
29    2    
31    3    
34    3    
37    2    
38    3    
41    6    
43    3    
46    5    
47    5    
49    3    
50    3    
53    2    
54    5    
58    3    
59    2    
61    2    
62    3    
67    2    
71    7    
73    5    
74    5    
79    3    
81    2    
82    7    
83    2    
86    3    
89    3    
94    5    
97    5    
98    3    
101    2    
103    5    
106    3    
107    2    
109    6    
113    3    
118    11    
121    2    
122    7    
125    2    
127    3    
131    2    
134    7    
137    3    
139    2    
142    7    
146    5    
149    2    
151    6    
157    5    
158    3
162    5
163    2
166    5
167    5
169    2
173    2
178    3
179    2
181    2
191    19
193    5
194    5
197    2
199    3
202    3
206    5
211    2
214    5
218    11
223    3
226    3
227    2
];
for i = 1 : size(S,"r")
m = S(i,1);
a = S(i,2);
    tf = number_isprimroot(a,m);
	assert_checktrue(tf);
end
