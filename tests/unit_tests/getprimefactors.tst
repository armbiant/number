// Copyright (C) 2012 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

p = number_getprimefactors ( 120 );
assert_checkequal ( p , [2 3 5]' );
//
[p,e] = number_getprimefactors ( 120 );
assert_checkequal ( p , [2 3 5]' );
assert_checkequal ( e , [3 1 1]' );
//
[p,e,status] = number_getprimefactors ( 120 );
assert_checkequal ( p , [2 3 5]' );
assert_checkequal ( e , [3 1 1]' );
assert_checktrue ( status );


// Reference
// "Factorizations of b^n+/-1, b = 2, 3, 5, 6, 7,10, 11, 12
// Up to High Powers"
// Third Edition
// John Brillhart, D. H. Lehmer
// J. L. Selfridge, Bryant Tuckerman,
// and S. S. Wagstaff, Jr.

function assertfactor ( n , method )
    // Check that the number n is correctly factored with method.
    //
    [p,e,status] = number_getprimefactors ( n , method );
    assert_checkequal ( prod(p.^e) , n );
    pstr = strcat ( string ( p ) , " " )
    estr = strcat ( string ( e ) , " " )
    mprintf ( " n=%d, p=[%s], e=[%s]\n", n , pstr , estr )
    if (method<>"pollard-rho") then
    assert_checktrue ( status );
    end
    if (n==1) then
        return
    end
    if (method<>"pollard-rho") then
    for q = p(:)'
        isprime = number_isprime ( q )
        assert_checktrue ( isprime );
    end
    end
endfunction

function testfactor ( method )
    mprintf ( "Factoring method=%s\n", method )
    assertfactor ( 1 , method )

    if ( method == "pollard-rho" ) then
        // Skip [9,18,27] : known case of failure, with 0 as initial state of the random number generator.
        integermat = [-12 2 3 4 5 6 7 8 9 10 13 15 16 20 23 26 28 29 30]
        integermat = [integermat (2.^[7 13]-1)]
    else
        integermat = [-12 (2 : 30) (2.^(5:15)-1)]
    end
    for n = integermat
        assertfactor ( n , method )
    end
endfunction

//
// Deeper test
testfactor ( "fasttrialdivision" )
testfactor ( "trialdivision" )
testfactor ( "memorylesstd" )
testfactor ( "fermat" )
grand("setsd",0);
testfactor ( "pollard-rho" )

