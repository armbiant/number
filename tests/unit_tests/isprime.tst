// Copyright (C) 2008-2009 - INRIA - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- CLI SHELL MODE -->

function testprime ( method )
  // Test the given method on some particular integers.
  //
  mprintf ( "Testing method=%s\n", method )
  // Begin with a few numbers
  integerlist  = [1  2  3  4  5  6  7  8  9  10 73 32003];
  expectedlist = [%f %t %t %f %t %f %t %f %f %f %t %t].';
  computed = [];
  for i=1:size(integerlist,"*")
    //mprintf ( " i=%s\n", string ( integerlist ( i ) ) )
    [ computed , status ] = number_isprime ( integerlist ( i ) , method );
    expected = expectedlist ( i );
    assert_checkequal ( computed , expected );
    assert_checkequal ( status , %t );
  end
  // Test some even integers
  integerlist = 2 * ( 2 : 4 : 50 );
  expected = %f;
  for i=1:size(integerlist,"*")
    //mprintf ( " i=%s\n", string ( integerlist ( i ) ) )
    [ computed , status ] = number_isprime ( integerlist ( i ) , method );
    assert_checkequal ( computed , expected );
    assert_checkequal ( status , %t );
  end
  // Test some primes
  integerlist = [
      2      3          7         13         19     23     29 
     73     79         89        101        107    109    113 
    179    181        193        199        223    227    229 
    283    293        311        317        337    347    349 
    419    421        433        443        457    461    463 
    547    557        569        577        593    599    601 
    661    673        683        701        719    727    733 
    811    821        827        839        857    859    863 
    947    953        971        983        997   1009   1013 
  ];
  expected = %t;
  for i=1:size(integerlist,"*")
    //mprintf ( " i=%s\n", string ( integerlist ( i ) ) )
    [ computed , status ] = number_isprime ( integerlist ( i ) , method );
    assert_checkequal ( computed , expected );
    assert_checkequal ( status , %t );
  end
endfunction

// Check simple examples
// n, isprime
// isprime=0 => %f, isprime=1 => %t
testmat = [
  0 0
  1 0
  2 1
  7 1
  10 0
];
ntest = size(testmat,"r");
for k = 1 : ntest
  n = testmat(k,1);
  isprime=testmat(k,2);
  if ( isprime == 0 ) then
    isprimee = %f;
  else
    isprimee = %t;
  end
  [isprimec , status] = number_isprime(n);
  assert_checkequal ( isprimec , isprimee );
  assert_checkequal ( status , %t );
end
//
// Configure imax
// A case of failure: not enough iterations
[isprimec , status] = number_isprime(1013,[],2);
assert_checkequal ( isprimec , %f );
assert_checkequal ( status , %f );
//
// Generates an error
instr = "isprime = number_isprime(1013,[],2)";
ierr = execstr(instr,"errcatch");
errmsg = lasterror();
assert_checkequal ( ierr , 10000 );
assert_checkequal ( errmsg , "number_isprime: The algorithm failed. Try to increase the value of the imax option." );
//
// Test the verbose mode
algos = ["primes"
  "modulo"
  "6k"
  "30k"
  "210k"
  "massdivide"
  ];
for method = algos'
  mprintf("\nmethod=%s\n",method);
  isprime = number_isprime(10321,method,[],%t);
end
//
// A loop on some values
algos = ["primes"
  "modulo"
  "6k"
  "30k"
  "210k"
  "massdivide"
  ];
mprintf("   primes, modulo, 6k, 30k, 210k, massdivide\n");
for n = 1 : 20
  msg="";
  msg=msg+msprintf("n=%d ",n);
  k = 1;
  for method = algos'
    isprime(k) = number_isprime(n,method);
    msg=msg+msprintf("%s ",string(isprime(k)));
    k=k+1;
  end
  // The test pass if all the algorithm returns the same boolean:
  // * either all algos say that n is prime (all algos return %t),
  // * or all algos say that n is not prime (all algos return %f).
  computed = and(isprime==isprime(1));
  if ( computed ) then
    msg=msg+msprintf(" - Correct");
  else
    msg=msg+msprintf(" - Error");
  end
  assert_checkequal(computed,%t);
  msg=msg+msprintf("\n");
  mprintf("%s\n",msg);
end

// Test isprime in regular situation
testprime ( "primes" )
testprime ( "modulo" )
testprime ( "6k" )
testprime ( "30k" )
testprime ( "210k" )
testprime ( "massdivide" )

