// Copyright (C) 2012 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


fac = number_getfactors(6);
assert_checkequal ( fac , [1.    2.    3.    6.]' );
//
[fac,status] = number_getfactors(6);
assert_checkequal ( fac , [1.    2.    3.    6.]' );
assert_checktrue ( status );


function checkgetfactors(n)
    // Check that n/lst(i) is an integer,
    // for i=1,2,...,m
    lst = number_getfactors(n)
    m = size(lst,"*")
    for i = 1 : m
        r = n/lst(i)
        f = r - int(r)
        if (f<>0) then
            error(msprintf("%s: %d/lst(%d) is not an integer","checkgetfactors",n,i))
        end
    end
    // Check that all divisors of n are in lst
    for i = 1 : m
        r = n/i
        f = r - int(r)
        if (f==0) then
            k = find(lst==i)
            if (k==[]) then
                error(msprintf("%s: %d is a divisor of %d","checkgetfactors",i,n))
            end
        end

    end
endfunction

for n = 1 : 30
    checkgetfactors(n);
end
