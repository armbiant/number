// Copyright (C) 2008-2009 - INRIA - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// Reference
// Factorizations of bn+/-1,
// b = 2, 3, 5, 6, 7,10, 11, 12
// Up to High Powers
// Third Edition
// John Brillhart, D. H. Lehmer
// J. L. Selfridge, Bryant Tuckerman,
// and S. S. Wagstaff, Jr.

function assertfactor ( n , method )
    mprintf ( "    n=%d ", n )

    tic()
    [computed,status] = number_factor ( n , method );
    t = toc()

    assert_checkequal ( prod(computed), n );
    facstr = strcat ( string(computed), " " )
    mprintf ( " f=[%s]\n", facstr )

    if method <> "pollard-rho" then
        assert_checktrue ( status );
    end

    if n == 1 then
        return
    end

    for p = computed
        isprime = number_isprime ( p )
        assert_checkequal ( isprime , %t );
    end
endfunction

function testfactor ( method )
    mprintf ( "Factoring method=%s\n", method )
    assertfactor ( 1 , method )

    if method == "pollard-rho" then
        // Skip [9,18,27] : known case of failure, with 0 as initial state
        // of the random number generator.
        integermat = [-12 [2:30] ];
        integermat(find(integermat ==  9)) = [];
        integermat(find(integermat == 18)) = [];
        integermat(find(integermat == 27)) = [];
        integermat = [integermat (2 .^ [7 13] - 1)];
    else
        integermat = [-12 (2 : 30) (2.^(5:15)-1)]
    end
    for n = integermat
        assertfactor ( n , method )
    end
endfunction

//
// Deeper test
testfactor ( "fasttrialdivision" )
testfactor ( "trialdivision" )
testfactor ( "memorylesstd" )
testfactor ( "fermat" )
grand("setsd", 0);
testfactor ( "pollard-rho" )

//
// Make an error

instr = "number_factor ( 2^42-1 , ""fasttrialdivision"" , 2 )";
ierr = execstr(instr, "errcatch");
msgerr = lasterror();
assert_checkequal ( ierr , 10000 );
assert_checkequal ( msgerr , "number_factor: The algorithm failed. " ..
                           + "Try to increase the value of the imax option." );

//
// Avoid to generate an error

[result,status]=number_factor ( 2^42-1 , "fasttrialdivision" , 2 );
assert_checkequal ( result ,  [1 3 3 7 7 43 127 337 5419] );
assert_checkequal ( status , %f );

//
// Use various seeds for Pollard's rho
grand("setsd", 0);
[result, status] = number_factor(2^20-1,"pollard-rho",[],[],[1 3 5]);
// The exact result is [3 5 5 11 31 41], but this quite a
// performance for this randomized algorithm.
assert_checkequal ( result , [15.    31.    41.    55.] );
assert_checkequal ( status , %f );
//
grand("setsd", 0);
[result,status] = number_factor(2^20-1,"pollard-rho",[],[],-1);
// The exact result is [3 5 5 11 31 41], but this quite a
// performance for this randomized algorithm.
assert_checkequal ( result , [3.    11.    25.    31.    41.] );
assert_checkequal ( status , %f );

