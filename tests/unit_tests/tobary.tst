// Copyright (C) 2008 - INRIA - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt



d = number_tobary ( 4 );
assert_checkequal ( d , [1 0 0]' );
//
basis = 2;
digits = number_tobary ( 4 , basis );
assert_checkequal ( digits , [1 0 0]' );
//
digits = number_tobary ( 4 , basis , "littleendian" );
assert_checkequal ( digits , [1 0 0]' );
//
digits = number_tobary ( 4 , basis , "bigendian" );
assert_checkequal ( digits , [0 0 1]' );
//
digits = number_tobary ( 4 , basis , "littleendian" , 8 );
assert_checkequal ( digits , [0 0 0 0 0 1 0 0]' );
//
digits = number_tobary ( 4 , basis , "bigendian" , 8 );
assert_checkequal ( digits , [0 0 1 0 0 0 0 0]' );
//
digits = number_tobary ( 8 , basis , "bigendian" , 8 );
assert_checkequal ( digits , [0 0 0 1 0 0 0 0]' );
//
assert_checkequal ( number_tobary ( [] ) , [] );
//

