Number toolbox

Purpose
-------

The goal of this toolbox is to provide several Number theory related
algorithms.
The toolbox is based on macros.
The implementation uses Scilab's double precision integers,
therefore is limited to integers in the [-2^53,2^53] interval.

Features
--------

 * number_addgroupmod - Returns the additive group modulo m.
 * number_coprime - Checks if two numbers are relatively prime.
 * number_eulertotient - Returns Euler totient function
 * number_extendedeuclid - Solves a linear Diophantine equation (Bezout identity).
 * number_gcd - Greatest common divisor.
 * number_inversemod - Computes the modular multiplicative inverse.
 * number_isdivisor - Checks if a divides b.
 * number_lcm - Least common multiple.
 * number_powermod - Modular exponentiation.
 * number_productmod - Computes a*s modulo m.
 * number_solvelinmod - Solves a linear modular equation.
 * number_ulamspiral - Returns the Ulam spiral.

Conversion

 * number_barygui - Plots a gui to see the digits of a number.
 * number_baryguiclose - Closes the bary GUI.
 * number_bin2hex - Converts a binary string into a hexadecimal string.
 * number_frombary - Compute a number from a matrix of digits.
 * number_hex2bin - Converts an hexadecimal string into a binary string.
 * number_tobary - Decompose a number into arbitrary basis.

Factorization

 * number_factor - Factors a number.
 * number_getfactors - Compute the divisors of a number.
 * number_getprimefactors - Factors a number into primes and exponents.

Multiplicative Group

 * number_isprimroot - See if a is a primitive root modulo m.
 * number_multgroupmod - Returns the multiplicative group modulo m.
 * number_multorder - Computes the multiplicative order modulo m.
 * number_searchprimroot - Find a primitive root modulo m.

Prime numbers

 * number_isprime - Checks if a number is prime.
 * number_maximalprimegap - Returns the maximal prime gap.
 * number_multiprimality - Returns the number of prime factors of n.
 * number_primecount - Returns the value of the prime-counting function.
 * number_primes - Computes the list of all primes up to n.
 * number_primes100 - Returns a matrix containing the 100 first primes.
 * number_primes1000 - Returns a matrix containing the 1000 first primes.
 * number_primes10000 - Returns a matrix containing the 10 000 first primes.
 * number_primorial - Returns the product of all primes lower or equal to n
 * number_probableprime - Check if a number is a probable prime.
 * number_pseudoprime - Check if a number is a pseudo prime.

Special Numbers

 * number_carmichael - Returns some Carmichael numbers.
 * number_fermat - Returns the Fermat number 2^2^n + 1
 * number_mersenne - Returns the Mersenne number 2^n - 1

Dependencies
------------

 * This module depends on the helptbx module (to update help pages).
 * This module depends on the assert module.
 * This module depends on the dispmat module.
 * This module depends on the apifun module (>=0.2).
 * This module depends on the specfun module (>=0.4).


TODO
-------
 * remove the "littleendian" order option : can be performed with ($:-1:1)
 * Draw Ulam's spiral

Forge
-----

http://forge.scilab.org/index.php/p/number/

ATOMS
-------

http://atoms.scilab.org/toolboxes/number

Author
-------

 * Copyright (C) 2008-2009 - INRIA - Michael Baudin
 * Copyright (C) 2009-2010 - DIGITEO - Michael Baudin
 * Copyright (C) 2012 - Michael Baudin

Licence
-------

This toolbox is released under the CeCILL_V2 licence :

http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

Bibliography
------------
"The Art of Computer Programming", Donald Knuth, Volume 2,
Seminumerical Algorithms.

"Introduction to algorithms",
Cormen, Leiserson, Rivest, Stein, 2001, McGraw-Hill.

An Introduction to the Theory of Numbers, 5th Edition
Ivan Niven, Herbert S. Zuckerman, Hugh L. Montgomery
1991, John Wiley & Sons

The primitive root theorem, Amin Witno, 2012,
WON Series in Discrete Mathematics and Modern Algebra Volume 5
